//
//  RHAccountResponseModel.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHAccountResponseModel: Codable {
    // id
    var counselorId: Int?
    // 名字
    var name: String?
    // 性别
    var sex: Int?
    // ？？
    var jobNumber: String?
    // 微信
    var weixin: String?
    // 电子邮件
    var email: String?
    // QQ
    var qq: String?
    // 名片
    var businessCard: String?
    // 状态
    var status: Int?
    // token
    var responsiblePlace: String?
    // 头像
    var photo: String?
    
}
