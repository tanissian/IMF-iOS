//
//  RHTabBarRootViewController.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import URLNavigator

class RHTabBarRootViewController: UITabBarController {
    
    private let navigator: NavigatorType
    
    var housesVC: RHCustomNavigationController!
    var buyersVC: RHCustomNavigationController!
    var accountVC: RHCustomNavigationController!

    init(navigator: NavigatorType) {
        self.navigator = navigator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.tintColor = RHAppSkinColor.sharedInstance.mainColor;
        self.createViewController()
        self.createTabbarItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (!RHAppUserProfile.sharedInstance.hasLoggedIn()) {
            self.userLogin()
        }
    }
    
    @objc private func userLogin() {
        RHAppUserProfile.sharedInstance.cleanUp()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) { [weak self] in
            guard let weakSelf = self else {return}
            let nav: UINavigationController = weakSelf.selectedViewController as! UINavigationController
            nav.popToRootViewController(animated: true)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                weakSelf.selectedIndex = 0
                let vc = RHLoginViewController()
                weakSelf.present(RHCustomNavigationController(rootViewController: vc), animated: true, completion: nil)
            }
        }
    }

    private func createViewController() {
        self.housesVC = RHCustomNavigationController(rootViewController: self.navigator.viewController(for: THor_RHHousesVC)!)
        self.buyersVC = RHCustomNavigationController(rootViewController: self.navigator.viewController(for: THor_RHBuyersVC)!)
        self.accountVC = RHCustomNavigationController(rootViewController: self.navigator.viewController(for: THor_RHAccountVC)!)
        self.viewControllers = [self.housesVC,self.buyersVC, self.accountVC];
    }

    
    private func createTabbarItem() {
        for (index, item) in self.viewControllers!.enumerated() {
            if (item == self.housesVC) {
                self.createTabBarItemWithTitle(title: NSLocalizedString("HousesText", comment: ""), unSelectedImage: "housing", selectedImage: "housing_selected", tag: index)
            } else if (item == self.buyersVC) {
                self.createTabBarItemWithTitle(title: NSLocalizedString("BuyersText", comment: ""), unSelectedImage: "buyers", selectedImage: "buyers_selected", tag: index)
            } else if (item == self.accountVC) {
                self.createTabBarItemWithTitle(title: NSLocalizedString("AccountText", comment: ""), unSelectedImage: "account", selectedImage: "account_selected", tag: index)
            }
        }
    }
    
    private func createTabBarItemWithTitle(title: String, unSelectedImage: String, selectedImage: String, tag: Int)  {
        let unSelectedImage = UIImage(named: unSelectedImage)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let selectedImage = UIImage(named: selectedImage)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let tabBarItem: UITabBarItem?  = self.tabBar.items?[tag]
        tabBarItem?.title = title
        tabBarItem?.image = unSelectedImage
        tabBarItem?.selectedImage = selectedImage
        tabBarItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : RHAppSkinColor.sharedInstance.mainColor], for: .highlighted)
    }
}
