//
//  RHCustomNavigationController.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHCustomNavigationController: RHNavigationController {

    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        self.backButtonItemImage = UIImage(named: "nav_icon_back")
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backButtonItemImage = UIImage(named: "nav_icon_back")
    }
    
    public func setNavigtionBarShowdow() {
        self.addNormalShadowImage();
        self.setNavigationBarBackgroundImage(bgImage: UIImage.createImageWithColor(color: RHAppSkinColor.sharedInstance.normalNavigationBarColor))

    }
    
    public func hideNavigationBarShowdow() {
        self.removeShadowImage();
        self.setNavigationBarBackgroundImage(bgImage: UIImage.createImageWithColor(color: RHAppSkinColor.sharedInstance.normalNavigationBarColor))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBarBackgroundImage(bgImage: UIImage.createImageWithColor(color: RHAppSkinColor.sharedInstance.normalNavigationBarColor))
    }
    
    override func navigationViewDidLoad() {
        let dict: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor : RHAppSkinColor.sharedInstance.emphasisTextColor,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18.0),
            NSAttributedStringKey.shadow: NSShadow()
        ]
        
        self.navigationBar.titleTextAttributes = dict;
        self.navigationBar.tintColor = RHAppSkinColor.sharedInstance.emphasisTextColor;
        //默认有
        self.addNormalShadowImage();
    }
}
