//
//  RHNavigationController.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHNavigationController: UINavigationController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    // 返回按钮图片
    public var backButtonItemImage: UIImage?
    
    public func navigationViewDidLoad() {
    }
    
    // 透明导航栏，默认字体白色
    public func showClearNavigationBar() {
        self.navigationBar.tintColor = UIColor.white;
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white];
        self.setNavigationBarBackgroundImage(bgImage: UIImage())
        self.removeShadowImage()
        
    }
    
    /**
     *  修改导航栏背景色
     *
     *  @param color       颜色
     *  @param tintColor   字体颜色
     *  @param translucent 是否半透明
     */
    public func showImageColorNavigationBarBackground(color: UIColor, textTintColor: UIColor, isTranslucent: Bool) {
        self.navigationBar.isTranslucent = isTranslucent;
        self.navigationBar.tintColor = textTintColor;
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: textTintColor]
        self.setNavigationBarBackgroundImage(bgImage: UIImage.createImageWithColor(color: color))
        self.removeShadowImage()
    }
    
    /**
     *  显示默认导航栏
     *
     *  @param tintColor 字体颜色
     */
    public func showNormalNavigationBarTintColor(tintColor: UIColor) {
        self.navigationBar.tintColor = tintColor;
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: tintColor];
        self.setNavigationBarBackgroundImage(bgImage: UIImage.createImageWithColor(color: UIColor.white))
        
        let dic: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor : RHAppSkinColor.sharedInstance.emphasisTextColor,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 20.0),
            NSAttributedStringKey.shadow: NSShadow()
        ]
        
        
        self.navigationBar.titleTextAttributes = dic;
        self.navigationBar.tintColor = RHAppSkinColor.sharedInstance.emphasisTextColor
    }
    
    /**
     *  设置NavigationBar的背景图片
     *
     *  @param bgImage 图片
     */
    public func setNavigationBarBackgroundImage(bgImage: UIImage?) {
        self.navigationBar.setBackgroundImage(bgImage, for: UIBarMetrics.default)
    }
    
    // 添加线条
    public func addNormalShadowImage() {
        self.navigationBar.shadowImage = UIImage.createImageWithColor(color: RHAppSkinColor.sharedInstance.placeholderTextColor)
    }
    
    // 去除底部线条
    public func removeShadowImage() {
        self.navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationViewDidLoad();
        self.interactivePopGestureRecognizer?.delegate = self;
    }
    
    override var childViewControllerForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
    
    @objc private func popself() {
        self.popViewController(animated: true)
    }
    
    func createBackButton() -> UIBarButtonItem {
        let backBtn : UIBarButtonItem = UIBarButtonItem(image: self.backButtonItemImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(popself))
        return backBtn
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super .pushViewController(viewController, animated: animated)
        if (viewController.navigationItem.leftBarButtonItem == nil && self.viewControllers.count > 1) {
            viewController.navigationItem.leftBarButtonItem = self.createBackButton()
        }
    }
}
