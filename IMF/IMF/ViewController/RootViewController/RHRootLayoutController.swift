//
//  RHRootLayoutController.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import TangramKit

class RHRootLayoutController: UIViewController {

    var rootLayout = TGLinearLayout(.vert)
    
    override func loadView() {
        let rootScrollView = UIScrollView()
    
        self.view = rootScrollView
        self.view.backgroundColor = RHAppSkinColor.sharedInstance.viewBackgroundColor

        let rootLayout = TGLinearLayout(.vert)
        rootLayout.tg_width.equal(.fill)
        rootLayout.tg_height.equal(.wrap)
        rootLayout.tg_gravity = TGGravity.horz.fill
        rootScrollView.addSubview(rootLayout)
        self.rootLayout = rootLayout
    }
}
