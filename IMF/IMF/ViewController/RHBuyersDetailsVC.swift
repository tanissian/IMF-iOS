//
//  RHBuyersDetailsVC.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/15.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import SnapKit

class RHBuyersDetailsVC: RHViewController, UITableViewDelegate, UITableViewDataSource {
    var tableView: RHTableView?
    let array:[String] = ["姓名", "手机", "邮箱", "城市", "备注"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("BuyersDetailsTitle", comment: "")
        loadTableView()
    }
    
    func loadTableView() {
        self.tableView = RHTableView(frame: CGRect.zero, style: .plain)
        self.tableView?.delegate = self;
        self.tableView?.dataSource = self;
        self.tableView?.register(RHKeyValueTableViewCell.self, forCellReuseIdentifier: "RHKeyValueTableViewCell")
        self.view.addSubview(self.tableView!)
        
        self.tableView?.snp.makeConstraints { [weak self](make) in
            guard let weakSelf = self else { return }
            make.top.equalTo(10)
            make.left.equalTo(weakSelf.view.snp.left)
            make.right.equalTo(weakSelf.view.snp.right)
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(weakSelf.view.snp.bottom)
            } else {
                make.bottom.equalTo(weakSelf.view.snp.bottom).offset(-49.0)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RHKeyValueTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RHKeyValueTableViewCell", for: indexPath) as! RHKeyValueTableViewCell
        cell.leftLabel?.text = array[indexPath.row]
        cell.rightLabel?.text = "xxx xxx xxx xxxx"
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 75.0))
        var phoneButton = UIButton()
        phoneButton = UIButton.xz_commonButtonWithTitle(title: NSLocalizedString("CallPhone", comment: ""), isEnabled: true)
        footerView.addSubview(phoneButton)
        
        phoneButton.snp.makeConstraints({(make) in
            make.top.equalTo(15.0)
            make.left.equalTo(20.0)
            make.right.equalTo(-20.0)
            make.height.equalTo(44)
        })
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 75.0
    }
}
