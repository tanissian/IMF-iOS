//
//  RHViewController.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/15.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = RHAppSkinColor.sharedInstance.viewBackgroundColor
    }
}
