//
//  RHRecommendedHousing.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/29.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import SVProgressHUD

class RHRecommendedHousingVC: RHViewController, UITableViewDelegate, UITableViewDataSource {
    var tableView: RHTableView?
    var clientId: Int?
    
    var model: RHRecommendedHousingModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("RecommendedHousingTitle", comment: "")
        loadTableView()
        loadApi()
    }
    func loadApi() {
        SVProgressHUD.show()
        RHAPIManager.sharedInstance.doGetRecommendHouse(clientId: String(clientId!), pageNumber: "1", pageSize: "10", result: {[weak self] (msg, data) in
            guard let weakSelf = self else { return }
            SVProgressHUD.dismiss()
            weakSelf.model = data as? RHRecommendedHousingModel
            weakSelf.tableView?.reloadData()
        }) { (code, msg) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showInfo(withStatus: msg)
        }
    }
    func loadTableView() {
        self.tableView = RHTableView(frame: CGRect.zero, style: .plain)
        self.tableView?.delegate = self;
        self.tableView?.dataSource = self;
        self.tableView?.register(RHRecommendedHousingTC.self, forCellReuseIdentifier: "RHRecommendedHousingTC")
        self.view.addSubview(self.tableView!)
        
        self.tableView?.snp.makeConstraints { [weak self](make) in
            guard let weakSelf = self else { return }
            make.top.equalTo(10)
            make.left.equalTo(weakSelf.view.snp.left)
            make.right.equalTo(weakSelf.view.snp.right)
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(weakSelf.view.snp.bottom)
            } else {
                make.bottom.equalTo(weakSelf.view.snp.bottom).offset(-49.0)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model == nil ? 0 : (self.model!.data?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RHRecommendedHousingTC = tableView.dequeueReusableCell(withIdentifier: "RHRecommendedHousingTC", for: indexPath) as! RHRecommendedHousingTC
        cell.bedRightLabel?.text = String((self.model?.data![indexPath.row].bed)!)
        cell.bathRightLabel?.text = String((self.model?.data![indexPath.row].bath)!)
        cell.priceRightLabel?.text = String((self.model?.data![indexPath.row].price)!)
        cell.creationTimeRightLabel?.text = self.model?.data![indexPath.row].createTime
        cell.addressRightLabel?.text = self.model?.data![indexPath.row].address
        cell.remarkRightLabel?.text = self.model?.data![indexPath.row].remark

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.delete
    }
    
    //在这里修改删除按钮的文字
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return NSLocalizedString("DeleteText", comment: "")
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            SVProgressHUD.show()
            RHAPIManager.sharedInstance.doDeleteRecommendHouse(clientId: String(self.clientId!), houseId: (self.model?.data![indexPath.row].houseId)!, result: { [weak self] (msg, data) in
                guard let weakSelf = self else { return }
                SVProgressHUD.dismiss()
                weakSelf.loadApi()
            }) { (code, msg) in
                SVProgressHUD.dismiss()
                SVProgressHUD.showInfo(withStatus: msg)
            }
        }
    }
}
