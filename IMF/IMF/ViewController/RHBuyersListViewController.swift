//
//  RHBuyersListViewController.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/29.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import SVProgressHUD

class RHBuyersListViewController: RHViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var tableView: RHTableView?
    var responseModel: RHBuyersDetailsResponseModel?
    var clientId: Int?
    var houseId = String()
    var imageArray = Array<UIImage>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("BuyersTitle", comment: "")
        
        loadTableView()
        loadClientsApi()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_icon_back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancel))
    }
    
    @objc func cancel() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func loadClientsApi(){
        SVProgressHUD.show()
        RHAPIManager.sharedInstance.doGetClients(pageNumber: "1", pageSize: "10", result: {[weak self] (msg, model) in
            guard let weakSelf = self else { return }
            SVProgressHUD.dismiss()
            weakSelf.responseModel = model as? RHBuyersDetailsResponseModel
            weakSelf.loadImageView()
            weakSelf.tableView?.reloadData()
        }) { (code, msg) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showInfo(withStatus: msg)
        }
    }
    
    func loadImageView(){
        for item in (self.responseModel?.data)! {
            if item.photo != nil {
                let data = try? Data(contentsOf: URL(string: item.photo!)!)
                if data != nil {
                    imageArray.append(UIImage(data: data!)!)
                }else{
                    imageArray.append(UIImage(named: "placeholder")!)
                }
            }else{
                imageArray.append(UIImage(named: "placeholder")!)
            }
        }
    }
    
    func loadTableView() {
        self.tableView = RHTableView(frame: CGRect.zero, style: .plain)
        self.tableView?.delegate = self;
        self.tableView?.dataSource = self;
        self.tableView?.register(RHImageTableViewCell.self, forCellReuseIdentifier: "RHImageTableViewCell")
        self.view.addSubview(self.tableView!)
        
        self.tableView?.snp.makeConstraints { [weak self](make) in
            guard let weakSelf = self else { return }
            make.top.equalTo(10)
            make.left.equalTo(weakSelf.view.snp.left)
            make.right.equalTo(weakSelf.view.snp.right)
            make.bottom.equalTo(weakSelf.view.snp.bottom)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.responseModel == nil ? 0 : (self.responseModel?.data?.count)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = RHRecommendedHousingSubmitVC()
        vc.houseId = self.houseId
        vc.clientId = (self.responseModel?.data![indexPath.row].clientId)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RHImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RHImageTableViewCell", for: indexPath) as! RHImageTableViewCell
        cell.leftImageView?.image = imageArray[indexPath.row]
        
        cell.titleLabel?.text = self.responseModel?.data![indexPath.row].name
        cell.contentLabel?.text = self.responseModel?.data![indexPath.row].phone
        return cell
    }
}
