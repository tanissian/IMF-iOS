//
//  RHRecommendedHousingSubmitVC.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/30.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import SVProgressHUD

class RHRecommendedHousingSubmitVC: RHViewController, UITextViewDelegate {

    var textView = UITextView()
    var houseId = String()
    var clientId = Int()
    var lable = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("BuyersTitle", comment: "")
        loadRemarkView()
    }
    
    func loadRemarkView() {
        let remarkView = UIView()
        let submit = UIButton.xz_commonButtonWithTitle(title: NSLocalizedString("SubmitText", comment: ""), isEnabled: true)
        submit.addTarget(self, action: #selector(submitButtonClick), for: .touchUpInside)
        textView.backgroundColor = UIColor.white
        textView.delegate = self
        lable = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        lable.text = "请输入备注信息"
        remarkView.addSubview(submit)
        remarkView.addSubview(textView)
        remarkView.addSubview(lable)
        self.view.addSubview(remarkView)

        textView.snp.makeConstraints({(make) in
            make.top.equalTo(10)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(150)
        })
        
        lable.snp.makeConstraints({(make) in
            make.top.equalTo(10)
            make.left.equalTo(10)
            make.right.equalTo(0)
            make.height.equalTo(30)
        })
        
        submit.snp.makeConstraints({(make) in
            make.top.equalTo(textView.snp.bottom).offset(15)
            make.left.equalTo(20.0)
            make.right.equalTo(-20.0)
            make.height.equalTo(44)
        })
        
        remarkView.snp.makeConstraints({(make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(submit.snp.bottom).offset(10)
        })
    }
    
    @objc func submitButtonClick() {
        SVProgressHUD.show()
        RHAPIManager.sharedInstance.doAddRecommendHouse(clientId: String(clientId), houseId: houseId, remark: textView.text, result: { (msg, data) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showInfo(withStatus: "推荐成功")
            self.dismiss(animated: true, completion: nil)
        }) { (code, msg) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showInfo(withStatus: msg)
        }
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        lable.text = ""
    }
}
