//
//  RHAddShareGroupVC.swift
//  IMF
//
//  Created by mac on 2018/10/11.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import SVProgressHUD

class RHAddShareGroupVC: RHViewController, UITableViewDelegate, UITableViewDataSource{
    var tableView: RHTableView?
    var submit = UIButton()
    var dataStorage = Array<Any>()

    var array = [NSLocalizedString("name", comment: ""),NSLocalizedString("note", comment: "")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("AddShareGroupText", comment: "")
        loadTableView()
    }
    
    func loadTableView() {
        self.tableView = RHTableView(frame: CGRect.zero, style: .plain)
        self.tableView?.delegate = self;
        self.tableView?.dataSource = self;
        self.tableView?.tableFooterView = addFooterView()
        self.tableView?.register(RHTextFieldTableCell.self, forCellReuseIdentifier: "RHTextFieldTableCell")
        
        self.view.addSubview(self.tableView!)
        dataStorage = Array<String>(repeating: "", count: array.count)
        self.tableView?.snp.makeConstraints { [weak self](make) in
            guard let weakSelf = self else { return }
            make.top.equalTo(10)
            make.left.equalTo(weakSelf.view.snp.left)
            make.right.equalTo(weakSelf.view.snp.right)
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(weakSelf.view.snp.bottom)
            } else {
                make.bottom.equalTo(weakSelf.view.snp.bottom).offset(-49.0)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RHTextFieldTableCell = tableView.dequeueReusableCell(withIdentifier: "RHTextFieldTableCell", for: indexPath) as! RHTextFieldTableCell
        cell.leftLabel?.text = array[indexPath.row]
        
        cell.rightTextField?.delegate = self
        cell.rightTextField?.tag = indexPath.row
        
        if dataStorage[indexPath.row] as! String != "" {
            cell.rightTextField?.text = dataStorage[indexPath.row] as? String
        }
        
        cell.rightTextField?.placeholder = "-"
        return cell
    }
    
    func addFooterView() -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: (self.tableView?.frame.width)!, height: 75.0))
        submit = UIButton.xz_commonButtonWithTitle(title:NSLocalizedString("SubmitText", comment: ""), isEnabled: false)
        footerView.addSubview(submit)
        submit.addTarget(self, action: #selector(submitButtonClick), for: .touchUpInside)
        submit.snp.makeConstraints({(make) in
            make.top.equalTo(15.0)
            make.left.equalTo(20.0)
            make.right.equalTo(-20.0)
            make.height.equalTo(44)
        })
        return footerView
    }
    
    @objc func submitButtonClick() {
        SVProgressHUD.show()
        RHAPIManager.sharedInstance.doAddShareGroups(name: dataStorage[0] as! String, remark: dataStorage[1] as! String, result: {[weak self] (msg, model) in
            guard let weakSelf = self else { return }
            SVProgressHUD.dismiss()
            weakSelf.navigationController?.popViewController(animated: true)
        }) { (code, msg) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showInfo(withStatus: msg)
        }
    }
    
    func dataValueChanged() {
        var isEnabledButton = true
        
        for item in self.dataStorage{
            if item as! String == ""{
                isEnabledButton = false
            }
        }
        submit.isEnabled = isEnabledButton
    }
}

extension RHAddShareGroupVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        dataStorage[textField.tag] = textField.text ?? String()
        dataValueChanged()
    }
}
