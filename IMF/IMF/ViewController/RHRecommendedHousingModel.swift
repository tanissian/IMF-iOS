//
//  RHRecommendedHousingModel.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/29.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHRecommendedHousingModel: Codable {
    var data: [RHRecommendedHousingDataModel]?
}

class RHRecommendedHousingDataModel: Codable {
    // 浴室
    var bath: Int?
    // 床
    var bed: Int?
    // 地址
    var address: String?
    // id
    var id: Int?
    // 创建时间
    var createTime: String?
    // houseId
    var houseId: String?
    // 价格
    var price: Int?
    // 头像
    var photo: String?
    // 备注
    var remark: String?
}
