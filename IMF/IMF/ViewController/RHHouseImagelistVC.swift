//
//  RHHouseImagelistVC.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/30.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHouseImagelistVC: RHViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    var collectionView: UICollectionView?
    let width = UIScreen.main.bounds.size.width//获取屏幕宽
    let height = UIScreen.main.bounds.size.height//获取屏幕高
    var imageDataArray: Array<String>?
    var imageArray: Array<UIImage>?
    var array : NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("housePhotoAlbum", comment: "")
        imageArray = Array()
        loadCollectionView()
        array = NSMutableArray()
        for _ in imageDataArray! {
            array?.add("")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
    }
    
    func loadCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width:(width - 60) / 3, height:(width - 60) / 3)
        layout.headerReferenceSize = CGSize.init(width: width - 30, height: 0)
        
        self.collectionView  = UICollectionView(frame: CGRect(x: 15, y: 0, width: width - 30, height: height - 70), collectionViewLayout: layout)
        self.collectionView! .register(RHImageViewCollectionViewCell.self, forCellWithReuseIdentifier:"RHImageViewCollectionViewCell")
        
        self.collectionView?.backgroundColor = RHAppSkinColor.sharedInstance.viewBackgroundColor
        self.view.addSubview(self.collectionView!)
        self.collectionView?.dataSource = self
        self.collectionView?.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = RHToViewPhotosVC()
        vc.index = indexPath.row
        vc.imageArray = self.array
        self.present(vc, animated: false, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (imageDataArray?.count)!
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RHImageViewCollectionViewCell", for: indexPath) as! RHImageViewCollectionViewCell
        cell.imageView?.image = UIImage(named: "placeholder")
        if (imageArray?.count)! <= indexPath.row {
            let url = URL(string: imageDataArray![indexPath.row])
            let request =  URLRequest(url: url!)
            let session  = URLSession.shared
            let dataTask =  session.dataTask(with: request, completionHandler: {
                (data, response, taskError) -> Void in
                if taskError != nil{
                    print(taskError.debugDescription)
                }else{
                    DispatchQueue.main.async {
                        self.imageArray!.append(UIImage(data: data!)!)
                        self.array?.replaceObject(at: indexPath.row, with: UIImage(data: data!)!)
                        cell.imageView?.image = UIImage(data: data!)
                    }
                }
            }) as URLSessionTask
            dataTask.resume()
        }else{
            cell.imageView?.image = imageArray![indexPath.row]
        }
//        cell.imageView?.backgroundColor = UIColor.red
        return cell
    }
}
