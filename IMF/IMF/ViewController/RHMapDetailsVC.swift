//
//  RHMapDetailsVC.swift
//  IMF
//
//  Created by mac on 2018/10/16.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import Mapbox
import SVProgressHUD

class RHMapDetailsVC: RHViewController, MGLMapViewDelegate {
    var mapView:MGLMapView?
    var houseId = String()
    var lat = Float()
    var lng = Float()
    let array = NSMutableArray()
    var annotationViewColor = UIColor.red
    var schoolmodel = RHSchoolsModel()
    var poiModel = RHPoiModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "周边详情"
        loadMapView()
        loadSchoolsApi()
        loadDoHomePoi()
        SVProgressHUD.show()
    }
    
    func loadDoHomePoi() {
        SVProgressHUD.show()
        RHAPIManager.sharedInstance.doHomePoi(houseId: houseId, result: { [weak self] (msg, model) in
            guard let weakSelf = self else { return }
            SVProgressHUD.dismiss()
            weakSelf.poiModel = model as! RHPoiModel
            weakSelf.loadPoiAnnotations()
        }) { (code, msg) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showInfo(withStatus: msg)
        }
    }
    
    func loadSchoolsApi() {
        SVProgressHUD.show()
        RHAPIManager.sharedInstance.doHomeSchools(houseId: houseId, result: { [weak self] (msg, model) in
            guard let weakSelf = self else { return }
            SVProgressHUD.dismiss()
            weakSelf.schoolmodel = model as! RHSchoolsModel
            weakSelf.loadSchoolsAnnotations()
        }) { (code, msg) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showInfo(withStatus: msg)
        }
    }
    
    func loadMapView(){
        mapView = MGLMapView(frame:CGRect.zero , styleURL: URL(string: RHAppConfig.sharedInstance.mapboxURL))
        self.view.addSubview(mapView!)
        mapView?.delegate = self
        
        mapView!.setCenter(CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng)), zoomLevel: 14, animated: false)
        
        self.mapView?.snp.makeConstraints { [weak self](make) in
            guard let weakSelf = self else { return }
            make.top.equalTo(0)
            make.left.equalTo(weakSelf.view.snp.left)
            make.right.equalTo(weakSelf.view.snp.right)
            make.bottom.equalTo(weakSelf.view.snp.bottom)
        }
        mapView?.showsUserLocation = true
        mapView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func loadPoiAnnotations(){
        let poiArray = NSMutableArray()
        if  self.poiModel.data != nil{
            for item in self.poiModel.data! {
                let cLLocationCoordinate2D:CLLocationCoordinate2D?
                let pointAnnotation = MGLPointAnnotation()
                pointAnnotation.title = item.name
                pointAnnotation.subtitle = item.address
                cLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(item.latitude!), CLLocationDegrees(item.longitude!))
                pointAnnotation.coordinate = cLLocationCoordinate2D!
                poiArray.add(pointAnnotation)
            }
        }
        annotationViewColor = UIColor.green
        self.mapView?.addAnnotations(poiArray as! [MGLAnnotation])
    }
    
    func loadSchoolsAnnotations() {
        let schoolArray = NSMutableArray()
        if self.schoolmodel.elementary != nil {
            for item in self.schoolmodel.elementary! {
                let cLLocationCoordinate2D:CLLocationCoordinate2D?
                let pointAnnotation = MGLPointAnnotation()
                pointAnnotation.title = item.name
                pointAnnotation.subtitle = item.address
                cLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(item.lat!), CLLocationDegrees(item.lng!))
                pointAnnotation.coordinate = cLLocationCoordinate2D!
                schoolArray.add(pointAnnotation)
            }
        }
        
        if self.schoolmodel.middle != nil {
            for item in self.schoolmodel.middle! {
                let cLLocationCoordinate2D:CLLocationCoordinate2D?
                let pointAnnotation = MGLPointAnnotation()
                pointAnnotation.title = item.name
                pointAnnotation.subtitle = item.address
                cLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(item.lat!), CLLocationDegrees(item.lng!))
                pointAnnotation.coordinate = cLLocationCoordinate2D!
                schoolArray.add(pointAnnotation)
            }
        }
        
        if self.schoolmodel.high != nil {
            for item in self.schoolmodel.high! {
                let cLLocationCoordinate2D:CLLocationCoordinate2D?
                let pointAnnotation = MGLPointAnnotation()
                pointAnnotation.title = item.name
                pointAnnotation.subtitle = item.address
                cLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(item.lat!), CLLocationDegrees(item.lng!))
                pointAnnotation.coordinate = cLLocationCoordinate2D!
                schoolArray.add(pointAnnotation)
            }
        }
        loadAnnotations()
        
        annotationViewColor = UIColor.blue
        self.mapView?.addAnnotations(schoolArray as! [MGLAnnotation])
    }
    
    func loadAnnotations() {
        var cLLocationCoordinate2D:CLLocationCoordinate2D?
        let pointAnnotation = MGLPointAnnotation()
        cLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(lat), CLLocationDegrees(lng))
        pointAnnotation.coordinate = cLLocationCoordinate2D!
        array.add(pointAnnotation)
        pointAnnotation.title = "我的位置"
        annotationViewColor = UIColor.red
        self.mapView?.addAnnotations(array as! [MGLAnnotation])
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        loadAnnotations()
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func mapView(_ mapView: MGLMapView, didAdd annotationViews: [MGLAnnotationView]) {
        SVProgressHUD.dismiss()
    }
    
    //自定义大头针
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        if annotation is MGLUserLocation && mapView.userLocation != nil {
            return nil
        }
        let Identifier = "MGLAnnotationView" + (annotation.title)!!
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: Identifier)
        
        if self.poiModel.data != nil {
            for item in self.poiModel.data! {
                if item.name == annotation.title{
                    if item.categories[0] == "Education" || item.categories[0] == "Entertainment"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_entertainment")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Automotive"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Automotive")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Beauty"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_beauty")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Education"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_education")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Event Planning" || item.categories[0] == "Services"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_service")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "nancial Services"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_financial")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Food"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Food")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Health" || item.categories[0] == "Medical"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_health")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Home Services"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_housekeeping")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Hotels" || item.categories[0] == "Travel"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_hotel")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Local Events" || item.categories[0] == "Flavor"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_localLife")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Local Media"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_LocalMedia")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Local Services"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_localService")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Nightlife"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Nightlife")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Pets"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Pets")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Professional Services"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Professional")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Public Services" || item.categories[0] == "Government"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Government")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Real Estate"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_ealEstate")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Recreation"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Recreation")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Religious Organizations"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Organizations")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Restaurants"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Restaurants")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Shopping"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Shopping")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }else if item.categories[0] == "Travel"{
                        if annotationView == nil {
                            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
                            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
                            annotationView?.backgroundColor = UIColor(patternImage: UIImage(named:"icon_Travel")!)
                            annotationView!.layer.masksToBounds = true
                        }
                        return annotationView
                    }
                }
            }
        }
        if annotationView == nil {
            annotationView = MGLAnnotationView.init(annotation: annotation, reuseIdentifier: Identifier)
            annotationView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24 )
            annotationView!.backgroundColor = annotationViewColor
            annotationView!.layer.cornerRadius = 12
            annotationView!.layer.masksToBounds = true
            annotationView!.layer.borderColor = (UIColor.white).cgColor
            annotationView!.layer.borderWidth = 3
        }
        return annotationView
    }
    
    func addImage(annotationView:MGLAnnotationView,annotation: MGLAnnotation) {

    }
}

