//
//  RHAccountViewController.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import SnapKit
import SVProgressHUD

class RHAccountViewController: RHViewController, UITableViewDelegate, UITableViewDataSource {
    var tableView: RHTableView?
    var accountResponseModel: RHAccountResponseModel?
    let array:[String] = ["姓名", "性别", "微信", "QQ", "邮箱","管理分享组"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("AccountTitle", comment: "")
        loadTableView()
        loadAccountApi()
    }
    
    func loadAccountApi(){
        SVProgressHUD.show()
        RHAPIManager.sharedInstance.doAccounts(result: { [weak self] (msg, model) in
            guard let weakSelf = self else { return }
            SVProgressHUD.dismiss()
            weakSelf.accountResponseModel = model as? RHAccountResponseModel
            weakSelf.tableView?.reloadData()
        }) { (code, msg) in
            SVProgressHUD.dismiss()
        }
    }
    
    func loadTableView() {
        self.tableView = RHTableView(frame: CGRect.zero, style: .plain)
        self.tableView?.delegate = self;
        self.tableView?.dataSource = self;
        self.tableView?.register(RHKeyValueTableViewCell.self, forCellReuseIdentifier: "RHKeyValueTableViewCell")
        self.view.addSubview(self.tableView!)
        self.tableView?.tableFooterView = addFooterView()
        
        self.tableView?.snp.makeConstraints { [weak self](make) in
            guard let weakSelf = self else { return }
            make.top.equalTo(10)
            make.left.equalTo(weakSelf.view.snp.left)
            make.right.equalTo(weakSelf.view.snp.right)
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(weakSelf.view.snp.bottom)
            } else {
                make.bottom.equalTo(weakSelf.view.snp.bottom).offset(-49.0)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RHKeyValueTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RHKeyValueTableViewCell", for: indexPath) as! RHKeyValueTableViewCell
        cell.leftLabel?.text = array[indexPath.row]
        if accountResponseModel != nil {
            if indexPath.row == 0{
                cell.rightLabel?.text = accountResponseModel?.name
            } else if indexPath.row == 1{
                cell.rightLabel?.text = accountResponseModel?.sex == 1 ? "男" : "女"
            } else if indexPath.row == 2{
                cell.rightLabel?.text = accountResponseModel?.weixin
            }else if indexPath.row == 3{
                cell.rightLabel?.text = accountResponseModel?.qq
            }else if indexPath.row == 4{
                cell.rightLabel?.text = accountResponseModel?.email
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5 {
            self.present(RHCustomNavigationController(rootViewController: RHShareGroupListVC()), animated: true, completion: nil)
        }
    }
    
    func addFooterView() -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 75.0))
        var exitLoginButton = UIButton()
        exitLoginButton = UIButton.xz_commonButtonWithTitle(title: NSLocalizedString("exitLogin", comment: ""), isEnabled: true)
        exitLoginButton.addTarget(self, action: #selector(exitLoginButtonClick), for: .touchUpInside)

        footerView.addSubview(exitLoginButton)
        
        let versionLable = UILabel.xz_labelWithFontSize(size: 13, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        versionLable.text = "版本号:" + (Bundle.main.infoDictionary!["CFBundleVersion"] as? String)!
        versionLable.textAlignment = NSTextAlignment.center
        footerView.addSubview(versionLable)
        
        exitLoginButton.snp.makeConstraints({(make) in
            make.top.equalTo(15.0)
            make.left.equalTo(20.0)
            make.right.equalTo(-20.0)
            make.height.equalTo(44)
        })
        
        versionLable.snp.makeConstraints({(make) in
            make.top.equalTo(70)
            make.left.equalTo(20.0)
            make.right.equalTo(-20.0)
            make.height.equalTo(40)
        })
        return footerView
    }
    
    @objc func exitLoginButtonClick() {
        let alertController = UIAlertController(title: "",
                                                message: "将登出此账号，是否进⾏?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "确定", style: .default, handler: {
            action in
            RHAppUserProfile.sharedInstance.cleanUp()
            let vc = RHLoginViewController()
            self.present(RHCustomNavigationController(rootViewController: vc), animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
