//
//  RHShareGroupListVC.swift
//  IMF
//
//  Created by mac on 2018/10/10.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import SVProgressHUD

class RHShareGroupListVC: RHViewController, UITableViewDelegate, UITableViewDataSource {
    var tableView: RHTableView?
    var shareGroupListModel = RHShareGroupListModel()
    var houseId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("ShareGroupText", comment: "")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_icon_back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancel))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "app_icon_add"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(addData))
        
        loadTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadDoShareGroupsApi()
    }
    
    @objc func cancel() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func addData() {
        self.navigationController?.pushViewController(RHAddShareGroupVC(), animated: true)
    }
    
    func loadDoShareGroupsApi(){
        SVProgressHUD.show()
        RHAPIManager.sharedInstance.doShareGroups(ageNumber: 1, pageSize: 20, result: {[weak self] (msg, model) in
            guard let weakSelf = self else { return }
            SVProgressHUD.dismiss()
            weakSelf.shareGroupListModel = model as! RHShareGroupListModel
            weakSelf.tableView?.reloadData()
        }) { (code, msg) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showInfo(withStatus: msg)
        }
    }
    
    func loadTableView() {
        self.tableView = RHTableView(frame: CGRect.zero, style: .plain)
        self.tableView?.delegate = self;
        self.tableView?.dataSource = self;
        self.tableView?.register(RHShareGroupListTableViewCell.self, forCellReuseIdentifier: "RHShareGroupListTableViewCell")
        
        self.view.addSubview(self.tableView!)
        self.tableView?.snp.makeConstraints {[weak self](make) in
            guard let weakSelf = self else { return }
            make.top.equalTo(10)
            make.left.equalTo(weakSelf.view.snp.left)
            make.right.equalTo(weakSelf.view.snp.right)
            make.bottom.equalTo(weakSelf.view.snp.bottom)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.shareGroupListModel.data != nil ? ((self.shareGroupListModel.data?.count)!) : 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return NSLocalizedString("DeleteText", comment: "")
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            SVProgressHUD.show()
            RHAPIManager.sharedInstance.doDeleteShareGroups(shareId: self.shareGroupListModel.data![indexPath.row].shareId!, result: {[weak self] (msg, model) in
                guard let weakSelf = self else { return }
                SVProgressHUD.dismiss()
                weakSelf.loadDoShareGroupsApi()
            }) { (code, msg) in
                SVProgressHUD.dismiss()
                SVProgressHUD.showInfo(withStatus: msg)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if houseId != "" {
            SVProgressHUD.show()
            RHAPIManager.sharedInstance.doAddHousingShareGroups(houseId: houseId, result: {[weak self] (msg, model) in
                guard let weakSelf = self else { return }
                SVProgressHUD.dismiss()
                SVProgressHUD.showInfo(withStatus: msg)
                weakSelf.dismiss(animated: true, completion: nil)
            }) { (code, msg) in
                SVProgressHUD.dismiss()
                SVProgressHUD.showInfo(withStatus: msg)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RHShareGroupListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RHShareGroupListTableViewCell", for: indexPath) as! RHShareGroupListTableViewCell
        cell.titleLabel?.text = self.shareGroupListModel.data![indexPath.row].name
        cell.createTimeLabel?.text = self.shareGroupListModel.data![indexPath.row].ctime
        cell.modifyTimeLabel?.text = self.shareGroupListModel.data![indexPath.row].utime == nil ? "暂无修改" : self.shareGroupListModel.data![indexPath.row].utime
        cell.noteLabel?.text = self.shareGroupListModel.data![indexPath.row].remark == nil ? "暂无备注" : self.shareGroupListModel.data![indexPath.row].remark
        return cell
    }
}
