//
//  RHDatePicker.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/22.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHDatePicker: NSObject {

    var datePicker = UIDatePicker()
    var cancelButton = UIButton()
    var confirmButton = UIButton()
    var view = UIView()
    let width = UIScreen.main.bounds.size.width
    let height = UIScreen.main.bounds.size.height
    let viewHeight = CGFloat(300.0)
    let animatTime = 0.35
    
    func RH_CreateDatePicker(viewController : UIViewController) {
        view.backgroundColor = UIColor.white
        cancelButton = UIButton.xz_buttonWithFontSize(size: 18, title: "取消", textColor: RHAppSkinColor.sharedInstance.normalButtonColor, backgroundColor: nil)
        confirmButton = UIButton.xz_buttonWithFontSize(size: 18, title: "确定", textColor: RHAppSkinColor.sharedInstance.normalButtonColor, backgroundColor: nil)
        cancelButton.addTarget(self, action: #selector(cancelButtonClick), for: .touchUpInside)
        
        view.addSubview(cancelButton)
        view.addSubview(confirmButton)
        view.addSubview(datePicker)
        datePicker.datePickerMode = .date
        viewController.view.addSubview(view)
        
        view.snp.makeConstraints({(make) in
            make.top.equalTo(height)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(width)
            make.height.equalTo(viewHeight)
        })
        
        cancelButton.snp.makeConstraints({(make) in
            make.top.equalTo(5.0)
            make.left.equalTo(10.0)
            make.width.equalTo(80)
            make.height.equalTo(40)
        })
        
        confirmButton.snp.makeConstraints({(make) in
            make.top.equalTo(5.0)
            make.right.equalTo(-10.0)
            make.width.equalTo(80)
            make.height.equalTo(40)
        })
        
        datePicker.snp.makeConstraints({(make) in
            make.top.equalTo(50.0)
            make.left.equalTo(0.0)
            make.width.equalTo(width)
            make.height.equalTo(viewHeight)
        })
    }
    
    @objc func cancelButtonClick() {
        hideView()
    }
    
    func showView(){
        UIView .animate(withDuration: self.animatTime, animations: {
            self.view.frame = CGRect(x: 0, y: self.height - self.viewHeight, width: self.width, height: self.viewHeight)
        })
    }
    
    func hideView(){
        UIView .animate(withDuration: self.animatTime, animations: {
            self.view.frame = CGRect(x: 0, y: self.height, width: self.width, height: self.viewHeight)
        })
    }
    
}
