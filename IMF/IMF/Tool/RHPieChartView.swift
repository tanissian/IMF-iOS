//
//  RHPieChartView.swift
//  IMF
//
//  Created by mac on 2018/10/19.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import Charts

class RHPieChartView: NSObject {
    let width = UIScreen.main.bounds.size.width
    var pieChartView: PieChartView?
    func RH_CreatePieChartView(view : UIView, description: String, data: NSArray) {
        pieChartView = PieChartView.init(frame: CGRect.init(x: 0, y: 0, width: width, height: width));
        pieChartView!.backgroundColor = UIColor.init(red: 230/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1.0);
        pieChartView!.setExtraOffsets(left: 40, top: 20, right: 40, bottom: 30);//设置这块饼的位置
        pieChartView!.chartDescription?.text = description;//描述文字
        pieChartView!.chartDescription?.font = UIFont.systemFont(ofSize: 12.0);//字体
        pieChartView!.chartDescription?.textColor = UIColor.black;//颜色
        pieChartView!.usePercentValuesEnabled = true;//转化为百分比
        pieChartView!.dragDecelerationEnabled = false;//我把拖拽效果关了
        pieChartView!.drawEntryLabelsEnabled = false;//显示区块文本
        pieChartView!.entryLabelFont = UIFont.systemFont(ofSize: 10);//区块文本的字体
        pieChartView!.entryLabelColor = UIColor.white;
        pieChartView!.drawSlicesUnderHoleEnabled = true;
        pieChartView!.drawHoleEnabled = true;//这个饼是空心的
        pieChartView!.holeRadiusPercent = 0.382//空心半径黄金比例
        pieChartView!.holeColor = UIColor.white;//空心颜色设置为白色
        pieChartView!.transparentCircleRadiusPercent = 0.4;//半透明空心半径
        pieChartView!.drawCenterTextEnabled = true;//显示中心文本
        //        _pieChartView.centerText = "饼状图";//设置中心文本,你也可以设置富文本`centerAttributedText`
        
        //图例样式设置
        pieChartView!.legend.maxSizePercent = 0.8;//图例的占比
        pieChartView!.legend.form = .square//图示：原、方、线
        pieChartView!.legend.formSize = 10;//图示大小
        pieChartView!.legend.formToTextSpace = 10;//文本间隔
        pieChartView!.legend.font = UIFont.systemFont(ofSize: 10);
        pieChartView!.legend.textColor = UIColor.gray;
        pieChartView!.legend.horizontalAlignment = .left;
        pieChartView!.legend.verticalAlignment = .bottom;
        pieChartView!.animate(xAxisDuration: 1.0, yAxisDuration: 1.0, easingOption: .easeInBack);
        
        view.addSubview(pieChartView!)
        drawPieChartView(data: data)
    }
    
    func drawPieChartView(data: NSArray) {
        var yVals = [PieChartDataEntry]();
        let dataSet = PieChartDataSet()
        for i in 0...data.count-1 {
            let entry = PieChartDataEntry.init(value: Double(((data[i] as! NSDictionary)["data"] as! Float)), label: ((data[i] as! NSDictionary)["title"] as! String));
            yVals.append(entry);
            dataSet.colors.append(((data[i] as! NSDictionary)["color"] as! UIColor))
        }
        
        dataSet.label = ""
        dataSet.values = yVals
        //设置名称和数据的位置 都在内就没有折线了哦
        dataSet.xValuePosition = .insideSlice;
        dataSet.yValuePosition = .outsideSlice;
        dataSet.sliceSpace = 1;//相邻块的距离
        dataSet.selectionShift = 6.66;//选中放大半径
        //指示折线样式
        dataSet.valueLinePart1OffsetPercentage = 0.75 //折线中第一段起始位置相对于区块的偏移量, 数值越大, 折线距离区块越远
        dataSet.valueLinePart1Length = 0.8 //折线中第一段长度占比
        dataSet.valueLinePart2Length = 0.3 //折线中第二段长度最大占比
        dataSet.valueLineVariableLength = true
        dataSet.valueLineWidth = 1 //折线的粗细
        dataSet.valueLineColor = UIColor.brown //折线颜色
        
        let data = PieChartData.init(dataSets: [dataSet]);
        //        data.setValueFormatter(VDChartAxisValueFormatter.init());//格式化值（添加个%）
        data.setValueFont(UIFont.systemFont(ofSize: 10.0));
        data.setValueTextColor(UIColor.lightGray);
        pieChartView!.data = data;
    }
}
