//
//  RHAlertController.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import SVProgressHUD

class RHAlertController: NSObject {
    func RH_LoadAlertController(viewController : UIViewController) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = viewController as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePickerController.allowsEditing = true
        
        let  alertController = UIAlertController(title:  "", message: "上传头像",preferredStyle: .actionSheet)
        let cancelAction =  UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let archiveAction = UIAlertAction(title: "拍照", style: .default) { (UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
                viewController .present(imagePickerController, animated: true, completion: nil)
            }else{
                SVProgressHUD.showInfo(withStatus: NSLocalizedString("请先打开相机权限", comment: ""))
            }
        }
        let deleteAction = UIAlertAction(title: "从相册选择", style: .default) { (UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
                viewController .present(imagePickerController, animated: true, completion: nil)
            }else{
                SVProgressHUD.showInfo(withStatus: NSLocalizedString("请先打开相册权限", comment: ""))
            }
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        alertController.addAction(archiveAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
}

