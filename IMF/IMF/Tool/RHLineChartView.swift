//
//  RHlineChartView!.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import Charts

class RHlineChartView: NSObject {
    let width = UIScreen.main.bounds.size.width
    var lineChartView: LineChartView?
    func RH_CreatelineChartView(view : UIView, description: String, data: NSArray, xValues: NSArray) {
        lineChartView = LineChartView.init(frame: CGRect.init(x: 0, y: 0, width: width, height: width));
//        lineChartView!.delegate = self as! ChartViewDelegate;//
        lineChartView!.backgroundColor = UIColor.init(red: 230/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1.0);
        lineChartView!.doubleTapToZoomEnabled = false;
        lineChartView!.scaleXEnabled = false;
        lineChartView!.scaleYEnabled = false;
        lineChartView!.chartDescription?.text = description//设置为""隐藏描述文字
        
        lineChartView!.noDataText = "暂无数据";
        lineChartView!.noDataTextColor = UIColor.gray;
        lineChartView!.noDataFont = UIFont.boldSystemFont(ofSize: 14);
        
        //y轴
        lineChartView!.rightAxis.enabled = false;
        let leftAxis = lineChartView!.leftAxis;
        leftAxis.labelCount = 10;
        leftAxis.forceLabelsEnabled = false;
        leftAxis.axisLineColor = UIColor.black;
        leftAxis.labelTextColor = UIColor.black;
        leftAxis.labelFont = UIFont.systemFont(ofSize: 10);
        leftAxis.labelPosition = .outsideChart;
        leftAxis.gridColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1.0);//网格
        leftAxis.gridAntialiasEnabled = false;//抗锯齿
//        leftAxis.axisMaximum = 100;//最大值
//        leftAxis.axisMinimum = 0;
        leftAxis.labelCount = 11;//多少等分
        
        //x轴
        let xAxis = lineChartView!.xAxis;
        xAxis.granularityEnabled = true;
        xAxis.labelTextColor = UIColor.black;
        xAxis.labelFont = UIFont.systemFont(ofSize: 10.0);
        xAxis.labelPosition = .bottom;
        xAxis.gridColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1.0);
        xAxis.axisLineColor = UIColor.black;
        xAxis.labelCount = 12;
        view.addSubview(lineChartView!)
        drawLineChart1(data: data, xValues: xValues)
    }
    
    func drawLineChart1(data: NSArray,xValues: NSArray){
        lineChartView!.xAxis.valueFormatter = VDChartAxisValueFormatter.init(xValues);
        lineChartView!.leftAxis.valueFormatter = VDChartAxisValueFormatter.init();
        
        var lineChartDataSet = [LineChartDataSet]()
        for i in 0...data.count - 1 {
            var yDataArray = [ChartDataEntry]();
            
            for j in 0...((data[i] as! NSDictionary)["data"] as! NSArray).count - 1 {
                let entry = ChartDataEntry.init(x: Double(j), y: Double((((data[i] as! NSDictionary)["data"] as! NSArray)[j] as! Float)))
                yDataArray.append(entry)
            }
            let set = LineChartDataSet.init(values: yDataArray, label: ((data[i] as! NSDictionary)["title"] as! String))
            set.setColor((data[i] as! NSDictionary)["color"] as! UIColor)
                        set.drawCirclesEnabled = false;//绘制转折点
                        set.lineWidth = 1.0;
            lineChartDataSet.append(set)
        }
        
        lineChartView!.data = LineChartData.init(dataSets: lineChartDataSet);
        lineChartView!.animate(xAxisDuration: 1.0, yAxisDuration: 1.0, easingOption: .easeInBack);
    }
    
    func drawLineChart(data: NSArray){
        let xValues = ["最低","最高"];
        lineChartView!.xAxis.valueFormatter = VDChartAxisValueFormatter.init(xValues as NSArray);
        lineChartView!.leftAxis.valueFormatter = VDChartAxisValueFormatter.init();

        var yDataArray1 = [ChartDataEntry]();
        
        for i in 0...xValues.count-1 {
            let entry = ChartDataEntry.init(x: Double(i), y: Double(231));
            yDataArray1.append(entry);
        }
        let set1 = LineChartDataSet.init(values: yDataArray1, label: "橙色");
        set1.colors = [UIColor.orange];
        set1.drawCirclesEnabled = false;//绘制转折点
        set1.lineWidth = 1.0;
        
        var yDataArray2 = [ChartDataEntry]();
        for i in 0...xValues.count-1 {
            if i == 0 {
                let entry = ChartDataEntry.init(x: Double(i), y: Double(234));
                yDataArray2.append(entry);
                
            }else{
                let entry = ChartDataEntry.init(x: Double(i), y: Double(1234));
                yDataArray2.append(entry);
                
            }
        }
        let set2 = LineChartDataSet.init(values: yDataArray2, label: "绿色");
        set2.colors = [UIColor.green];
        set2.drawCirclesEnabled = false;
        set2.lineWidth = 1.0;
        
        let data = LineChartData.init(dataSets: [set1,set2]);
        
        lineChartView!.data = data;
        lineChartView!.animate(xAxisDuration: 1.0, yAxisDuration: 1.0, easingOption: .easeInBack);
    }
}

class VDChartAxisValueFormatter: NSObject,IAxisValueFormatter {
    var values:NSArray?;
    override init() {
        super.init();
    }
    init(_ values: NSArray) {
        super.init();
        self.values = values;
    }
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if values == nil {
            return "\(value)";
        }
        return values?.object(at: Int(value)) as! String;
    }
}
