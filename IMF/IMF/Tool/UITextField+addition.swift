//
//  UITextField+addition.swift
//  JiuCheYue
//
//  Created by 刘表聪 on 2018/3/28.
//  Copyright © 2018年 xinzhen. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    /**
     *  设置TextField
     *
     *  @param size              字体大小
     *  @param placeholderText   提示内容
     *  @param text              内容
     *  @param isShowClearButton 清除按钮
     */
    class func xz_textFieldWithFontSize(size: CGFloat, placeholderText: String?, text: String?, isShowClearButton: Bool) -> UITextField {
        let textField = UITextField()
        if (size != 0) {
            textField.font = UIFont.systemFont(ofSize: size)
        }
        textField.placeholder = placeholderText
        textField.text = text
        textField.clearButtonMode = isShowClearButton ? .whileEditing : .never
        return textField
    }
    
}
