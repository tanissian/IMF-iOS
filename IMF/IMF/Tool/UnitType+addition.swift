//
//  UnitType+addition.swift
//  JiuCheYue
//
//  Created by 刘表聪 on 2018/6/6.
//  Copyright © 2018年 xinzhen. All rights reserved.
//

import Foundation

extension String {
    func CNY() -> String {
        if self.count == 0 {
            return "¥0"
        }
        return "¥\(self)"
    }
    
    func checkCNYStr() -> String {
        if self.count == 0 {
            return ""
        }
        return "¥\(self)"
    }
    
    func boolStr() -> String {
        if self.count == 0 {
            return self
        }
        return self == "1" ? "是" : "否"
    }
    
    func type() -> String {
        if self.count == 0 {
            return self
        }
        if self == "1" {
            return "国内"
        } else if self == "2" {
            return "国外"
        } else {
            return self
        }
        
    }
    
    func allowStr() -> String {
        if self.count == 0 {
            return self
        }
        return self == "1" ? "allowed" : "not allowed"
    }
    
    func rate() -> String {
        if self.count == 0 {
            return ""
        }
        let i: Float = Float(self)!
        
        if i > Float(Int(i)) {
            return "\(String(format: "%.2f", i))%"
        }
        return "\(String(Int(i)))%"
    }
    
    func day() -> String {
        if self.count == 0 {
            return "0天"
        }
        return "\(self)天"
    }
    
    func priceItem() -> String {
        // 价格条款 1=CFR,2=FOB,3=CIF,4=FCA ,
        if self == "1" {
            return "CFR"
        } else if self == "2" {
            return "FOB"
        } else if self == "3" {
            return "CIF"
        } else if self == "4" {
            return "FCA"
        }
        return self
    }
    
    func moneyChinese() -> String {
        // 币种1=USD, 2=EUR,3=CAD,4=CNY(默认) ,
        if self == "1" {
            return "美元"
        } else if self == "2" {
            return "欧元"
        } else if self == "3" {
            return "加拿大元"
        } else if self == "4" {
            return "人民币"
        }
        return self
    }
    
    func moneyEnglish() -> String {
        if self == "1" {
            return "USD"
        } else if self == "2" {
            return "EUR"
        } else if self == "3" {
            return "CAD"
        } else if self == "4" {
            return "CNY"
        }
        return self
    }
    
    func moneyUnit() -> String {
        if self == "1" {
            return "$"
        } else if self == "2" {
            return "€"
        } else if self == "3" {
            return "C$"
        } else if self == "4" {
            return "¥"
        }
        return self
    }
    
    func businessType() -> String {
        // 101=国际TT付汇,102=国际返贷,201=国内大贸质押,202=国内垫税,203=国内垫资发车,204=国内代理采购
        if self == "101" {
            return "国际TT付汇"
        } else if self == "102" {
            return "国际返贷"
        } else if self == "201" {
            return "国内大贸质押"
        } else if self == "202" {
            return "国内垫税"
        } else if self == "203" {
            return "国内垫资发车"
        } else if self == "204" {
            return "国内代理采购"
        }
        return self
    }
    
    func timesStr() -> String {
        if self.count == 0 {
            return ""
        }
        return "\(self)次"
    }
    
    static func documnetName(documentType: Int) -> String {
        var documnetName = ""
        switch documentType {
        case 1:
            documnetName = "赎车凭证"
            break
        case 2:
            documnetName = "保证金缴纳凭证"
            break
        case 3:
            documnetName = "赎车到账凭证"
            break
        case 4:
            documnetName = "退费凭证"
            break
        case 5:
            documnetName = "海关退费凭证"
            break
        case 6:
            documnetName = "二次退费凭证"
            break
        case 7:
            documnetName = "TT协议"
            break
        case 8:
            documnetName = "合同"
            break
        case 9:
            documnetName = "发票"
            break
        case 10:
            documnetName = "车窗纸"
            break
        case 11:
            documnetName = "尽调报告"
            break
        case 12:
            documnetName = "尽调照片"
            break
        case 13:
            documnetName = "6个月企业流水"
            break
        case 14:
            documnetName = "进销存报告"
            break
        case 15:
            documnetName = "法人征信"
            break
        case 16:
            documnetName = "三证合一营业执照"
            break
        case 17:
            documnetName = "开户许可证"
            break
        case 18:
            documnetName = "经营地址权属证明"
            break
        case 19:
            documnetName = "人及实际控制人身份证"
            break
        case 20:
            documnetName = "办公场所照片"
            break
        case 21:
            documnetName = "公司/个人银行流水"
            break
        case 22:
            documnetName = "入库单"
            break
        case 23:
            documnetName = "车辆照片"
            break
        case 24:
            documnetName = "保证金到账凭证"
            break
        case 25:
            documnetName = "付汇确认回单"
            break
        case 26:
            documnetName = "对外付汇申请书"
            break
        case 27:
            documnetName = "结算单确认excel"
            break
        case 28:
            documnetName = "企业/个人征信报告"
            break
        case 30:
            documnetName = "垫税缴纳凭证"
            break
        case 31:
            documnetName = "垫税到账凭证"
            break
        case 32:
            documnetName = "垫税合同"
            break
        case 33:
            documnetName = "垫税-三税单"
            break
        case 34:
            documnetName = "垫税-保证金税单"
            break
        case 35:
            documnetName = "支付税款-缴纳凭证"
            break
        case 40:
            documnetName = "关单VIN码"
            break
        case 41:
            documnetName = "一致性证书VIN码"
            break
        case 42:
            documnetName = "商检VIN码"
            break
        case 43:
            documnetName = "环保清单"
            break
        case 44:
            documnetName = "放款凭证"
            break
        case 45:
            documnetName = "签约双方授权代表信息"
            break
        case 46:
            documnetName = "车辆经销合同"
            break
        case 47:
            documnetName = "车辆采购订单"
            break
        case 48:
            documnetName = "垫税-保证金水单"
            break
        case 50:
            documnetName = "补扫件"
            break
        case 51:
            documnetName = "经销合同"
            break
        case 52:
            documnetName = "返贷合同"
            break
        case 53:
            documnetName = "上游合同"
            break
        case 54:
            documnetName = "代理报关协议"
            break
        case 55:
            documnetName = "物流运单"
            break
        case 56:
            documnetName = "费用确认单"
            break
        case 57:
            documnetName = "委托代理进口协议"
            break
        default:
            break
        }
        return documnetName
    }
}
