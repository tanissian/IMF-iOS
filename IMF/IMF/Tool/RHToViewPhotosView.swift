//
//  RHToViewPhotosView.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/10/1.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHToViewPhotosView: NSObject {
    var scrollView = UIScrollView()
    var pageControl = UIPageControl()
    let width = UIScreen.main.bounds.size.width
    let height = UIScreen.main.bounds.size.height - 44
    
    func RH_LoadPhotosView(viewController : UIViewController, array: Array<UIImage>){
//        self.viewController = viewController
        viewController.view.addSubview(scrollView)
        scrollView.isPagingEnabled = true
        scrollView.alpha = 0

        var i = 0
        scrollView.snp.makeConstraints({(make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(width)
            make.height.equalTo(height)
        })
        
        for _ in array {
            let imageView = UIImageView()
            imageView.image = array[i]
            imageView.backgroundColor = UIColor.black
            imageView.contentMode = .scaleAspectFit
            scrollView.addSubview(imageView)
            
            imageView.snp.makeConstraints({(make) in
                make.top.equalTo(0)
                make.left.equalTo(Int(width) * i)
                make.width.equalTo(width)
                make.height.equalTo(height)
            })
            i += 1
        }
        
        scrollView.contentSize = CGSize(width: Int(width) * array.count, height: Int(height))
    }
    
    func showPhotosView() {
//        scrollView.isHidden = false
//        self.viewController.view.bringSubview(toFront: scrollView)

//        self.scrollView.
        scrollView.alpha = 1;
    }
    
    func hiddenPhotosView() {
        scrollView.alpha = 0;

//        scrollView.isHidden = true
    }
}
