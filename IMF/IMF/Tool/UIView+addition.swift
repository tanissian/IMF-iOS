//
//  UIView+addition.swift
//  JiuCheYue
//
//  Created by 刘表聪 on 2018/3/28.
//  Copyright © 2018年 xinzhen. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

let kBorderViewTag: Int = 12345

extension UIView {
    func xz_bottomBorderWithHeight(height: CGFloat, color colorBorder: UIColor?) -> UIView {
        let borderView = UIView()
        borderView.tag = kBorderViewTag
        borderView.backgroundColor = colorBorder
        self.addSubview(borderView)
        
        borderView.snp.makeConstraints {[weak self](make) in
            guard let strongSelf = self else { return }
            make.bottom.equalTo(strongSelf.snp.bottom)
            make.width.equalTo(strongSelf.snp.width)
            make.centerX.equalTo(strongSelf.snp.centerX)
            make.height.equalTo(height)
        }
        return borderView
    }
    
    func xz_topBorderWithHeight(height: CGFloat, color colorBorder: UIColor?) -> UIView {
        let borderView = UIView()
        borderView.tag = kBorderViewTag
        borderView.backgroundColor = colorBorder
        self.addSubview(borderView)
        
        borderView.snp.makeConstraints {[weak self](make) in
            guard let strongSelf = self else { return }
            make.top.equalTo(strongSelf.snp.top)
            make.width.equalTo(strongSelf.snp.width)
            make.centerX.equalTo(strongSelf.snp.centerX)
            make.height.equalTo(height)
        }
        return borderView
    }
    
    func xz_leftBorderWithWidth(width: CGFloat, color colorBorder: UIColor?) -> UIView {
        let borderView = UIView()
        borderView.tag = kBorderViewTag
        borderView.backgroundColor = colorBorder
        self.addSubview(borderView)
        
        borderView.snp.makeConstraints {[weak self](make) in
            guard let strongSelf = self else { return }
            make.left.equalTo(strongSelf.snp.left)
            make.height.equalTo(strongSelf.snp.height)
            make.centerY.equalTo(strongSelf.snp.centerY)
            make.width.equalTo(width)
        }
        return borderView
    }
    
    func xz_rightBorderWithWidth(width: CGFloat, color colorBorder: UIColor?) -> UIView {
        let borderView = UIView()
        borderView.tag = kBorderViewTag
        borderView.backgroundColor = colorBorder
        self.addSubview(borderView)
        
        borderView.snp.makeConstraints {[weak self](make) in
            guard let strongSelf = self else { return }
            make.right.equalTo(strongSelf.snp.right)
            make.height.equalTo(strongSelf.snp.height)
            make.centerY.equalTo(strongSelf.snp.centerY)
            make.width.equalTo(width)
        }
        return borderView
    }
    
    func xz_setBorderColor(color: UIColor) {
        let BorderView =  self.viewWithTag(kBorderViewTag)
        BorderView?.backgroundColor = color
    }
}
