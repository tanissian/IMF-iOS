//
//  UILabel+addition.swift
//  JiuCheYue
//
//  Created by 刘表聪 on 2018/3/28.
//  Copyright © 2018年 xinzhen. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    /**
     *  设置正常Label
     *
     *  @param size            大小
     *  @param textColor       颜色
     *  @param backgroundColor 背景色
     *  @param backgroundColor 行数
     */
    class func xz_labelWithFontSize(size: CGFloat, textColor: UIColor, backgroundColor: UIColor?, numberOfLines: Int) -> UILabel {
        return self.xz_labelWithFontSize(size: size, textColor: textColor, backgroundColor: backgroundColor, numberOfLines: numberOfLines, isBold: false)
    }
    
    /**
     *  设置粗体Label
     *
     *  @param size            字体大小
     *  @param textColor       字体颜色
     *  @param backgroundColor 背景色
     *  @param backgroundColor 行数
     */
    class func xz_labelWithBoldFontSize(size: CGFloat, textColor: UIColor, backgroundColor: UIColor?, numberOfLines: Int) -> UILabel {
       return self.xz_labelWithFontSize(size: size, textColor: textColor, backgroundColor: backgroundColor, numberOfLines: numberOfLines, isBold: true)
    }
    
    class private func xz_labelWithFontSize(size: CGFloat, textColor: UIColor, backgroundColor: UIColor?, numberOfLines: Int, isBold: Bool) -> UILabel {
        let label = UILabel()
        if (size != 0) {
            label.font = isBold ? UIFont.boldSystemFont(ofSize: size) : UIFont.systemFont(ofSize: size)
        }
        label.textColor = textColor
        label.lineBreakMode = .byCharWrapping
        label.numberOfLines = numberOfLines
        label.layer.backgroundColor = (backgroundColor != nil) ? backgroundColor!.cgColor : UIColor.clear.cgColor
        label.setContentCompressionResistancePriority(UILayoutPriority.required, for: UILayoutConstraintAxis.vertical)
        label.setContentHuggingPriority(UILayoutPriority.required, for: UILayoutConstraintAxis.vertical)
        return label
    }
    
    
}
