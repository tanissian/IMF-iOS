//
//  RHBarChartView.swift
//  IMF
//
//  Created by mac on 2018/10/20.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import Charts

class RHBarChartView: NSObject {
    let width = UIScreen.main.bounds.size.width
    var barChartView: BarChartView?
    var chartview:UIView?
    func RH_CreatebarChartView(view : UIView, description: String, data: NSMutableArray) {
        barChartView = BarChartView.init(frame: CGRect.init(x: 0, y: 0, width: width, height: width - 60));
        barChartView!.backgroundColor = UIColor.init(red: 230/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1.0);
        barChartView!.setExtraOffsets(left: 40, top: 20, right: 40, bottom: 30);//设置这块饼的位置
        barChartView!.chartDescription?.text = description;//描述文字
        barChartView!.chartDescription?.font = UIFont.systemFont(ofSize: 12.0);//字体
        barChartView!.chartDescription?.textColor = UIColor.black;//颜色
        barChartView!.dragDecelerationEnabled = false;//我把拖拽效果关了
        
        //图例样式设置
        barChartView!.legend.maxSizePercent = 0.8;//图例的占比
        barChartView!.legend.form = .none//图示：原、方、线
        barChartView!.legend.formSize = 10;//图示大小
        barChartView!.legend.formToTextSpace = 10;//文本间隔
        barChartView!.legend.font = UIFont.systemFont(ofSize: 10);
        barChartView!.legend.textColor = UIColor.gray;
        barChartView!.legend.horizontalAlignment = .left;
        barChartView!.legend.verticalAlignment = .top;
        barChartView!.animate(xAxisDuration: 1.0, yAxisDuration: 1.0, easingOption: .easeInBack);
        
        chartview = UIView(frame: CGRect.init(x: 0, y: 0, width: width, height: width))
        chartview!.backgroundColor = UIColor.init(red: 230/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1.0);
        let colorViewSize = 10
        var colorViewY = width - 60
        var colorViewX = 20
        for i in 0...data.count-1 {
            let labelWidth = ((data[i] as! NSDictionary)["title"] as! String).count * 6
            
            if colorViewX > Int(width - 90){
                colorViewX = 20
                colorViewY = colorViewY + CGFloat(colorViewSize + 5)
            }else{
                    colorViewX = colorViewX + colorViewSize + 5
            }
            let view = UIView(frame: CGRect(x: colorViewX, y: Int(colorViewY), width: colorViewSize, height: colorViewSize))
            view.backgroundColor = ((data[i] as! NSDictionary)["color"] as! UIColor)
            
            let label = UILabel.xz_labelWithFontSize(size: CGFloat(colorViewSize), textColor: UIColor.blue, backgroundColor: nil, numberOfLines: 0)
            let labelX = colorViewX + colorViewSize + 5
            label.frame = CGRect(x: labelX, y: Int(colorViewY), width: labelWidth, height: colorViewSize)
            colorViewX = colorViewX + labelWidth
            label.text = ((data[i] as! NSDictionary)["title"] as! String)
            chartview?.addSubview(view)
            chartview?.addSubview(label)
        }
        chartview!.addSubview(barChartView!)
        view.addSubview(chartview!)
        setBarChartViewData(data: data)
    }
    
    func setBarChartViewData(data: NSArray) {
        var dataEntris = [BarChartDataEntry]()
        let chartDataSet = BarChartDataSet()
        var color = [NSUIColor]()
        for i in 0...data.count-1 {
            let dataEntry = BarChartDataEntry(x:Double(i), y:  Double(((data[i] as! NSDictionary)["data"] as! Float)))
            color.append((data[i] as! NSDictionary)["color"] as! UIColor)
            dataEntris.append(dataEntry)
        }
        chartDataSet.colors = color
        chartDataSet.values = dataEntris
        let chartData = BarChartData(dataSet: chartDataSet)
        self.barChartView!.data = chartData
        self.barChartView!.animate(yAxisDuration: 1.0, easingOption: .easeInBounce)
    }
}
