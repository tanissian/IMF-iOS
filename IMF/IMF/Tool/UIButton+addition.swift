//
//  UIButton+addition.swift
//  JiuCheYue
//
//  Created by 刘表聪 on 2018/3/28.
//  Copyright © 2018年 xinzhen. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    /*
     *  公用的button
     *
     *  @param title      按钮title
     *  @param isEnabled  初始化是否可以点击
     */
    class func xz_commonButtonWithTitle(title: String?, isEnabled: Bool) -> UIButton {
        let button = UIButton()
        button.setNormalBackground(normalColor: RHAppSkinColor.sharedInstance.normalButtonColor, highlightedColor: RHAppSkinColor.sharedInstance.highlightedButtonColor, disabledColor: RHAppSkinColor.sharedInstance.disableButtonColor)
        button.setTitle(title, for: UIControlState.normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 5
        button.isEnabled =  isEnabled
        return button
    }
    
    /*
     *  @param size                  字体size
     *  @param title                 按钮title
     *  @param textColor             title颜色
     *  @param backgroundGroundColor 按钮背景颜色
     */
    class func xz_buttonWithFontSize(size: CGFloat, title: String?, textColor: UIColor?, backgroundColor: UIColor?) -> UIButton {
        let button = UIButton()
        
        button.setTitle(title, for: .normal)
        button.setTitleColor(textColor, for: .normal)
        
        if (size != 0) {
            button.titleLabel?.font = UIFont.systemFont(ofSize: size)
        }
        
        if (backgroundColor != nil) {
            button.setBackgroundImage(UIImage.createImageWithColor(color: backgroundColor!), for: .normal)
        }
        button.setContentCompressionResistancePriority(UILayoutPriority.required, for: UILayoutConstraintAxis.vertical)
        button.setContentCompressionResistancePriority(UILayoutPriority.required, for: UILayoutConstraintAxis.horizontal)
        button.setContentHuggingPriority(UILayoutPriority.required, for: UILayoutConstraintAxis.horizontal)
        button.setContentHuggingPriority(UILayoutPriority.required, for: UILayoutConstraintAxis.vertical)
        return button
    }
    
    /**
     *  设置背景颜色 三合一
     *
     *  @param normalColor  正常颜色
     *  @param hightedColor 高亮颜色
     *  @param disableColor 不可点击颜色
     */
    func setNormalBackground(normalColor: UIColor, highlightedColor: UIColor, disabledColor: UIColor) {
        self.setNormalBackground(normalColor: normalColor)
        self.setHighlightedBackground(highlightedColor: highlightedColor)
        self.setDisableClickBackground(disableColor: disabledColor)
    }
    
    /**
     *  设置正常的按钮颜色  按钮可点击
     *
     *  @param normalColor 颜色色值
     */
    func setNormalBackground(normalColor: UIColor) {
        self.setBackgroundImage(UIImage.createImageWithColor(color: normalColor), for: .normal)
    }
    
    /**
     *  设置选中以后的颜色  按钮可点击
     *
     *  @param hightedColor 颜色色值
     */
    func setHighlightedBackground(highlightedColor: UIColor) {
        self.setBackgroundImage(UIImage.createImageWithColor(color: highlightedColor), for: .highlighted)
    }
    
    /**
     *  设置按钮不可点击的颜色 按钮不可点击
     *
     *  @param disableColor 颜色色值
     */
    func setDisableClickBackground(disableColor: UIColor) {
        self.setBackgroundImage(UIImage.createImageWithColor(color: disableColor), for: .disabled)
    }
}
