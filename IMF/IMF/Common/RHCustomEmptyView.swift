//
//  XZCustomEmptyView.swift
//  JiuCheYue
//
//  Created by 刘表聪 on 2018/4/11.
//  Copyright © 2018年 xinzhen. All rights reserved.
//

import UIKit

class RHCustomEmptyView: UIView {
    
    var infoView: UIView = UIView()
    
    var emptyImageView: UIImageView = UIImageView()
    
    var describeLabel: UILabel =  UILabel()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.createView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.createView()
    }
    
    private func createView() {
        self.addSubview(self.infoView)
        
        self.infoView.addSubview(self.emptyImageView)
        
        self.describeLabel.adjustsFontSizeToFitWidth = true;
        self.describeLabel.font =  UIFont.systemFont(ofSize: 15.0)
        self.describeLabel.textColor = RHAppSkinColor.sharedInstance.secondaryTextColor
        self.describeLabel.textAlignment = .center
        self.infoView.addSubview(self.describeLabel)
        
        self.infoView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 200, height: 200))
        }
        
        self.emptyImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize(width: 150, height: 150))
        }
        
        self.describeLabel.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-10.0)
            make.left.right.equalToSuperview()
        }
    }
    
    func updateViewImage(image: UIImage?, description: String?) {
        self.emptyImageView.image = image;
        self.describeLabel.text = description;
    }
}
