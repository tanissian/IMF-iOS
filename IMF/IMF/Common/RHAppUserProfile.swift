//
//  RHAppUserProfile.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

let RHUserToken = "RHUserToken"

class RHAppUserProfile: NSObject {
    // 用户登陆标识
    var userToken: String!
    
    // MARK: @singleton
    static let sharedInstance: RHAppUserProfile = { RHAppUserProfile() }()
    
    override init() {
        super.init()
        let userDefaults = UserDefaults.standard
        self.userToken = userDefaults.string(forKey: RHUserToken)
    }
    
    func hasLoggedIn() -> Bool {
        let userDefaults = UserDefaults.standard
        let token = userDefaults.string(forKey: RHUserToken)
        return token != nil && token!.count > 0
    }
    
    func save(token: String?) {
        self.userToken = token
        let userDefaults = UserDefaults.standard
        userDefaults.set(token, forKey: RHUserToken)
        userDefaults.synchronize()
    }
    
    func cleanUp() {
        self.userToken = nil
        let userDefaults = UserDefaults.standard
        userDefaults.set(nil, forKey: RHUserToken)
        userDefaults.synchronize()
    }
}
