//
//  RHAppConfig.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/19.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHAppConfig: NSObject {
    /* App Name */
    var appName: String!
    
    /* mapbox URL */
    var mapboxURL: String!
    
    /* WeChat share title */
    var wShareTitle: String!
    
    /* WeChat share description*/
    var wShareDescription: String!
    
    /* PDF file Extension */
    var PDFExtension: String!
    
    /* 网络请求密码 */
    var apiPassword: String!
    
    static let sharedInstance: RHAppConfig = { RHAppConfig() }()
    
    override init() {
        super.init()
        appName = "IMF"
        mapboxURL = "mapbox://styles/mapbox/streets-v10"
        wShareTitle = "IMF Title"
        wShareDescription = "IMF Share Description"
        PDFExtension = "pdf"
        apiPassword = "c303701a92932475b516e5033b7a3e0b"
    }
}
