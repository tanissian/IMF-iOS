//
//  RHValidateString.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/15.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHValidateString: NSObject {
    // MARK: @singleton
    static let sharedInstance: RHValidateString = { RHValidateString() }()
    
    override init() {
        super.init()
    }
    
    func isPhoneNumber(phone: String) -> Bool {

        let num = "1[3|5|7|8|][0-9]{9}"
        let numText = NSPredicate(format: "SELF MATCHES %@", num)
        
        return numText.evaluate(with: phone)
    }
}
