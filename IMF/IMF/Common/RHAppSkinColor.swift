//
//  RHAppSkinColor.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHAppSkinColor: NSObject {
    
    //---------------------------------全局---------------------------------
    /**
     * APP主色调，用于特别强调或突出的文字、背景、按钮和icon #F95A28
     */
    var mainColor: UIColor!
    
    /**
     *  重点金额色，用于数据记录里面的一些金额文字 #4ABE89
     */
    var emphasisAmountColor: UIColor!
    
    //---------------------------------背景用色-----------------------------
    /**
     *  背景色 #F5F5F5
     */
    var viewBackgroundColor: UIColor!
    
    //---------------------------分割线-----------------------
    
    /**
     *  分割线色
     */
    var separatorColor: UIColor!
    
    
    //--------------------------------文字用色---------------------
    
    /**
     *  导航栏颜色 #f9f9f9
     */
    var normalNavigationBarColor: UIColor!
    
    /**
     *  重点文字颜色 #333333
     */
    var emphasisTextColor: UIColor!
    
    /**
     *  次要文字，提示文字 #999999
     */
    var secondaryTextColor: UIColor!
    
    /**
     *  提示输入状态文字 #cccccc
     */
    var placeholderTextColor: UIColor!
    
    
    //------------------------------------按钮用色--------------------------
    /**
     *  按钮正常颜色
     */
    var normalButtonColor: UIColor!
    /**
     *  按钮选中以后高亮颜色
     */
    var highlightedButtonColor: UIColor!
    /**
     *  按钮不可点击颜色
     */
    var disableButtonColor: UIColor!
    
    // MARK: @singleton
    static let sharedInstance: RHAppSkinColor = { RHAppSkinColor() }()
    
    override init() {
        super.init()
        
        mainColor = UIColor.colorFromHexString(hexString: "#f95a28")
        emphasisAmountColor = UIColor.colorFromHexString(hexString: "#4ABE89")
        viewBackgroundColor = UIColor.colorFromHexString(hexString: "#efeff4")
        separatorColor = UIColor.colorFromHexString(hexString: "#dddddd")
        normalNavigationBarColor = UIColor.colorFromHexString(hexString: "#ffffff")
        emphasisTextColor = UIColor.colorFromHexString(hexString: "#333333")
        secondaryTextColor = UIColor.colorFromHexString(hexString: "#999999")
        placeholderTextColor = UIColor.colorFromHexString(hexString: "#cccccc")
        normalButtonColor = UIColor.colorFromHexString(hexString: "#f95a28")
        highlightedButtonColor = UIColor.colorFromHexString(hexString: "#ea4612")
        disableButtonColor = UIColor.colorFromHexString(hexString: "#cccccc")
    }
}
