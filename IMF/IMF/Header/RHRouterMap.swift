//
//  RHRouterMap.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import URLNavigator

enum RouterMap {
    static func initialize(navigator: NavigatorType) {
        //登录页面
        navigator.register(THor_RHLoninVC) { (url, values, context) -> UIViewController? in
            return RHLoginViewController()
        }
        
        //房源首页
        navigator.register(THor_RHHousesVC) { (url, values, context) -> UIViewController? in
            return RHHouserViewController()
        }
        
        //买家页面
        navigator.register(THor_RHBuyersVC) { (url, values, context) -> UIViewController? in
            return RHBuyersViewController()
        }
        
        //账户页面
        navigator.register(THor_RHAccountVC) { (url, values, context) -> UIViewController? in
            return RHAccountViewController()
        }
        
        //房源搜索
        navigator.register(THor_RHHousesSearchVC) { (url, values, context) -> UIViewController? in
            return RHHousesSearchViewController()
        }
        
        //添加买家客户
        navigator.register(THor_RHAddBuyersVC) { (url, values, context) -> UIViewController? in
            return RHAddBuyersCustomersVC()
        }
        
        //买家详情
        navigator.register(THor_RHBuyersDetailsVC) { (url, values, context) -> UIViewController? in
            return RHBuyersDetailsVC()
        }
        
        //房子详情
        navigator.register(THor_RHHouseDetailsVC) { (url, values, context) -> UIViewController? in
            return RHHouseDetailsVC()
        }
        
        //推荐房源
        navigator.register(THor_RHRecommendedHousingVC) { (url, values, context) -> UIViewController? in
            return RHRecommendedHousingVC()
        }
        
    }
}
