//
//  RHRouterNavigator.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
import URLNavigator

class RHRouterNavigator: NSObject {
    
    var navigator: NavigatorType!
    
    // MARK: @singleton
    static let sharedInstance: RHRouterNavigator = { RHRouterNavigator() }()
    
    override init() {
        super.init()
        let routerNavigator = Navigator()
        RouterMap.initialize(navigator: routerNavigator)
        navigator = routerNavigator
    }
}
