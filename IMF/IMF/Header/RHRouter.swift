//
//  RHRouter.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/12.
//  Copyright © 2018年 imf. All rights reserved.
//

import Foundation

// 登录
let THor_RHLoninVC = "thor://viewController/RHLoginViewController"
// 首页
let THor_RHHousesVC = "thor://viewController/RHHousesViewController"
// 买家
let THor_RHBuyersVC = "thor://viewController/RHBuyersViewController"
// 账户
let THor_RHAccountVC = "thor://viewController/RHAccountViewController"
// 房源搜索
let THor_RHHousesSearchVC = "thor://viewController/RHHousesSearchViewController"
// 添加买家客户
let THor_RHAddBuyersVC = "thor://viewController/RHAddBuyersCustomersVC"
// 买家详情
let THor_RHBuyersDetailsVC = "thor://viewController/RHBuyersDetailsVC"
// 房子详情
let THor_RHHouseDetailsVC = "thor://viewController/RHHouseDetailsVC"
// 推荐房源
let THor_RHRecommendedHousingVC = "thor://viewController/RHRecommendedHousingVC"

