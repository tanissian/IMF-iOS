//
//  RHConfiguration.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/14.
//  Copyright © 2018年 imf. All rights reserved.
//

import Foundation

// 处理网络请求错误通知
let RHAppErrorNotification = "RHAppErrorNotification"
// 登录通知
let RHLoginNotification = "RHLoginNotification"
