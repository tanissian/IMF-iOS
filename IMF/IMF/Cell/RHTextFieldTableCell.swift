//
//  RHTextFieldTableCell.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/15.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHTextFieldTableCell: RHTableViewCell {

    var leftLabel: UILabel?
    var rightTextField: UITextField?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.leftLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        self.leftLabel?.adjustsFontSizeToFitWidth = true
        self.contentView.addSubview(self.leftLabel!)
        self.rightTextField = UITextField.xz_textFieldWithFontSize(size: 15, placeholderText: nil, text: nil, isShowClearButton: true)
        self.contentView.addSubview(self.rightTextField!)
        
        self.leftLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(2.0)
            make.left.equalTo(20.0)
            make.width.equalTo(120.0)
            make.height.equalTo(40)
        })
        
        self.rightTextField?.snp.makeConstraints({ [weak self](make) in
            guard let weakSelf = self else {return}
            make.top.equalTo(weakSelf.leftLabel!.snp.top)
            make.left.equalTo(weakSelf.leftLabel!.snp.right).offset(5)
            make.right.equalTo(20)
            make.height.equalTo(weakSelf.leftLabel!.snp.height)
        })
    }
}
