//
//  RHContentTableViewCell.swift
//  IMF
//
//  Created by mac on 2018/10/9.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHContentTableViewCell: RHTableViewCell {
    var leftLabel: UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.leftLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 0)
        self.contentView.addSubview(self.leftLabel!)
    }
}
