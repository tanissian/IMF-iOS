//
//  RHHeadPortraitTableViewCell.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHeadPortraitTableViewCell: RHTableViewCell {

     var headPortraitImageView: UIImageView?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        headPortraitImageView = UIImageView()
        self.contentView.addSubview(headPortraitImageView!)
        headPortraitImageView?.contentMode =  UIViewContentMode.scaleToFill;

        headPortraitImageView?.layer.masksToBounds = true
        headPortraitImageView?.layer.cornerRadius = 80/2
        
        headPortraitImageView?.snp.makeConstraints({(make) in
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
            make.left.equalTo(20)
            make.width.equalTo(80)
            make.height.equalTo(80)
        })
    }
}
