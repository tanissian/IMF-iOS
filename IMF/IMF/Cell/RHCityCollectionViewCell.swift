//
//  RHCityCollectionViewCell.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/13.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHCityCollectionViewCell: UICollectionViewCell {
    let width = UIScreen.main.bounds.size.width
    
    var titleButton:UIButton?

    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        titleButton = UIButton.xz_buttonWithFontSize(size: 14, title: "", textColor: UIColor.black, backgroundColor: UIColor.white)
        titleButton?.frame = CGRect(x: 0, y: 5, width: width/3 - 20, height: 40)
        titleButton?.layer.masksToBounds = true
        titleButton?.layer.cornerRadius = 5
        self.addSubview(titleButton!)
    }
}
