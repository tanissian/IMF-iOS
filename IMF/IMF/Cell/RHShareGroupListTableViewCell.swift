//
//  RHShareGroupListTableViewCell.swift
//  IMF
//
//  Created by mac on 2018/10/10.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHShareGroupListTableViewCell: RHTableViewCell {
    var titleLabel: UILabel?
    var createTimeLabel: UILabel?
    var modifyTimeLabel: UILabel?
    var noteLabel: UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.titleLabel = UILabel.xz_labelWithFontSize(size: 15.0, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        var createTimeLeftLabel: UILabel?
        var modifyTimeLeftLabel: UILabel?
        var noteLeftLabel: UILabel?

        createTimeLeftLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        createTimeLeftLabel?.text = "创建时间"
        self.createTimeLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        modifyTimeLeftLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        modifyTimeLeftLabel?.text = "修改时间"
        self.modifyTimeLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        noteLeftLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        noteLeftLabel?.text = "备注"
        self.noteLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(self.titleLabel!)
        self.contentView.addSubview(createTimeLeftLabel!)
        self.contentView.addSubview(self.createTimeLabel!)
        self.contentView.addSubview(modifyTimeLeftLabel!)
        self.contentView.addSubview(self.modifyTimeLabel!)
        self.contentView.addSubview(noteLeftLabel!)
        self.contentView.addSubview(self.noteLabel!)
        
        self.titleLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(5.0)
            make.left.equalTo(20.0)
            make.width.equalTo(300.0)
            make.height.equalTo(20)
        })
        
        createTimeLeftLabel?.snp.makeConstraints({[weak self](make) in
            guard let weakSelf = self else {return}
            make.top.equalTo((weakSelf.titleLabel?.snp.bottom)!).offset(2)
            make.left.equalTo((weakSelf.titleLabel?.snp.left)!)
            make.width.equalTo(120.0)
            make.height.equalTo((weakSelf.titleLabel?.snp.height)!)
        })
        
        self.createTimeLabel?.snp.makeConstraints({ [weak self](make) in
            guard let weakSelf = self else {return}
            make.top.equalTo((weakSelf.titleLabel?.snp.bottom)!).offset(5)
            make.left.equalTo((createTimeLeftLabel?.snp.right)!).offset(10)
            make.right.equalTo(20)
            make.height.equalTo(createTimeLeftLabel!.snp.height)
        })
        
        modifyTimeLeftLabel?.snp.makeConstraints({[weak self](make) in
            guard let weakSelf = self else {return}
            make.top.equalTo((createTimeLeftLabel?.snp.bottom)!).offset(2)
            make.left.equalTo((weakSelf.titleLabel?.snp.left)!)
            make.width.equalTo(120.0)
            make.height.equalTo(createTimeLeftLabel!.snp.height)
        })
        
        self.modifyTimeLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((createTimeLeftLabel?.snp.bottom)!).offset(2)
            make.left.equalTo((createTimeLeftLabel?.snp.right)!).offset(10)
            make.right.equalTo(20)
            make.height.equalTo(createTimeLeftLabel!.snp.height)
        })
        
        noteLeftLabel?.snp.makeConstraints({[weak self](make) in
            guard let weakSelf = self else {return}
            make.top.equalTo((weakSelf.modifyTimeLabel?.snp.bottom)!).offset(2)
            make.left.equalTo((weakSelf.titleLabel?.snp.left)!)
            make.width.equalTo(120.0)
            make.height.equalTo(20)
        })
        
        self.noteLabel?.snp.makeConstraints({[weak self](make) in
            guard let weakSelf = self else {return}
            make.top.equalTo((noteLeftLabel?.snp.bottom)!).offset(2)
            make.left.equalTo((weakSelf.titleLabel?.snp.left)!)
            make.right.equalTo(20)
            make.height.equalTo(20)
        })
    }
}
