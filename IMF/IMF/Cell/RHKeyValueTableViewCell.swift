//
//  RHSearchTableViewCell.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/13.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHKeyValueTableViewCell: RHTableViewCell {
    var leftLabel: UILabel?
    var rightLabel: UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.leftLabel = UILabel.xz_labelWithFontSize(size: 14.0, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(self.leftLabel!)
        self.leftLabel?.adjustsFontSizeToFitWidth = true
        self.rightLabel = UILabel.xz_labelWithFontSize(size: 16.0, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(self.rightLabel!)
        self.rightLabel?.adjustsFontSizeToFitWidth = true

        
        self.leftLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(2.0)
            make.left.equalTo(20.0)
            make.width.equalTo(120.0)
            make.height.equalTo(40)
        })
        
        self.rightLabel?.snp.makeConstraints({ [weak self](make) in
            guard let weakSelf = self else {return}
            make.top.equalTo(weakSelf.leftLabel!.snp.top)
            make.left.equalTo(weakSelf.leftLabel!.snp.right).offset(5)
            make.right.equalTo(20)
            make.height.equalTo(weakSelf.leftLabel!.snp.height)
        })
    }
}
