//
//  RHImageViewCollectionViewCell.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/17.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHImageViewCollectionViewCell: UICollectionViewCell {
    let width = UIScreen.main.bounds.size.width
    
    var imageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView(){
        imageView = UIImageView()
        imageView?.frame = CGRect(x: 0, y: 5, width:(width - 60) / 3, height: (width - 60) / 3)
        self.addSubview(imageView!)
    }
}
