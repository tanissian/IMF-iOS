//
//  RHCollectionViewTableViewCell.swift
//  IMF
//
//  Created by mac on 2018/10/9.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHCollectionViewTableViewCell: RHTableViewCell {
    
    var collectionView: UICollectionView?
    let width = UIScreen.main.bounds.size.width//获取屏幕宽
    let height = UIScreen.main.bounds.size.height//获取屏幕高
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width:(width - 60) / 3, height:(width - 60) / 3)
        layout.headerReferenceSize = CGSize.init(width: width - 30, height: 0)
//        self.headerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height:self.photosModel.data == nil ? 0 : (self.photosModel.data?.count)! >= 9 ? width - 20 : (self.photosModel.data?.count)! > 6 ? width - 20 : (self.photosModel.data?.count)! > 4 ? (width - 20) / 3 * 2 : (width - 20) / 3))
//        self.collectionView  = UICollectionView(frame: CGRect(x: 15, y: 0, width: width - 30, height: 0, collectionViewLayout: layout))
        self.collectionView = UICollectionView(frame: CGRect(x: 15, y: 0, width: width - 30, height: 0),collectionViewLayout: layout)
        self.collectionView! .register(RHImageViewCollectionViewCell.self, forCellWithReuseIdentifier:"cell")
        self.collectionView! .register(RHCityCollectionReusableView.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: headerIdentifier)
        self.collectionView?.backgroundColor = RHAppSkinColor.sharedInstance.viewBackgroundColor
//        self.collectionView?.dataSource = self
//        self.collectionView?.delegate = self
        self.contentView.addSubview(self.collectionView!)
    }
}
