//
//  RHImageTableViewCell.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/17.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHImageTableViewCell: RHTableViewCell {
    
    var leftImageView: UIImageView?
    var titleLabel: UILabel?
    var contentLabel: UILabel?
    var imageWidth = Int(80)

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        leftImageView = UIImageView()
        leftImageView?.contentMode =  .scaleToFill;
        self.contentView.addSubview(leftImageView!)
        
        titleLabel = UILabel.xz_labelWithFontSize(size: 18, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(titleLabel!)
        
        contentLabel = UILabel.xz_labelWithFontSize(size: 14, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 0)
        self.contentView.addSubview(contentLabel!)
        
        leftImageView?.snp.makeConstraints({(make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageWidth)
        })
        
        titleLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(10)
            make.left.equalTo(imageWidth + 20)
            make.right.equalTo(-20)
        })
        
        contentLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(40)
            make.left.equalTo(imageWidth + 20)
            make.right.equalTo(-20)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
