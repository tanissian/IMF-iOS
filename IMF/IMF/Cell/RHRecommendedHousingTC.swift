//
//  RHRecommendedHousingTC.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/29.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHRecommendedHousingTC: RHTableViewCell {
    var leftImageView: UIImageView?
    var imageWidth = Int(80)
    
    var bedLeftLabel: UILabel?
    var bedRightLabel: UILabel?
    
    var bathLeftLabel: UILabel?
    var bathRightLabel: UILabel?
    
    var priceLeftLabel: UILabel?
    var priceRightLabel: UILabel?
    
    var creationTimeLeftLabel: UILabel?
    var creationTimeRightLabel: UILabel?
    
    var addressLeftLabel: UILabel?
    var addressRightLabel: UILabel?
    
    var remarkeLeftLabel: UILabel?
    var remarkRightLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        leftImageView = UIImageView()
        leftImageView?.contentMode =  UIViewContentMode.scaleToFill;
        leftImageView?.backgroundColor = UIColor.red
        self.contentView.addSubview(leftImageView!)
        
        bedLeftLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(bedLeftLabel!)
        bedLeftLabel?.text = "卧室数量:"
        bedRightLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 0)
        self.contentView.addSubview(bedRightLabel!)

        bathLeftLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(bathLeftLabel!)
        bathLeftLabel?.text = "浴室数量:"
        bathRightLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(bathRightLabel!)

        priceLeftLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(priceLeftLabel!)
        priceLeftLabel?.text = "价格:"
        priceRightLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(priceRightLabel!)

        creationTimeLeftLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(creationTimeLeftLabel!)
        creationTimeLeftLabel?.text = "创建时间:"
        creationTimeRightLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(creationTimeRightLabel!)

        addressLeftLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(addressLeftLabel!)
        addressLeftLabel?.text = "地址:"
        addressRightLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(addressRightLabel!)

        remarkeLeftLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.emphasisTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(remarkeLeftLabel!)
        remarkeLeftLabel?.text = "备注:"
        remarkRightLabel = UILabel.xz_labelWithFontSize(size: 15, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        self.contentView.addSubview(remarkRightLabel!)
        
        leftImageView?.snp.makeConstraints({(make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(imageWidth)
            make.height.equalTo(imageWidth)
        })
        
        bedLeftLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(10)
            make.left.equalTo(imageWidth + 20)
            make.width.equalTo(65)
        })
        
        bedRightLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(10)
            make.left.equalTo((bedLeftLabel?.snp.right)!).offset(10)
            make.width.equalTo(40)
        })
        
        bathLeftLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(10)
            make.left.equalTo((bedRightLabel?.snp.right)!).offset(20)
            make.width.equalTo(65)
        })
        
        bathRightLabel?.snp.makeConstraints({(make) in
            make.top.equalTo(10)
            make.left.equalTo((bathLeftLabel?.snp.right)!).offset(10)
            make.width.equalTo(65)
        })
        
        priceLeftLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((bedLeftLabel?.snp.bottom)!).offset(10)
            make.left.equalTo(imageWidth + 20)
            make.width.equalTo(65)
        })
        
        priceRightLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((bedLeftLabel?.snp.bottom)!).offset(10)
            make.left.equalTo((priceLeftLabel?.snp.right)!).offset(10)
            make.right.equalTo(-20)
        })
    
        creationTimeLeftLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((priceLeftLabel?.snp.bottom)!).offset(10)
            make.left.equalTo(imageWidth + 20)
            make.width.equalTo(65)
        })
        
        creationTimeRightLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((priceLeftLabel?.snp.bottom)!).offset(10)
            make.left.equalTo((creationTimeLeftLabel?.snp.right)!).offset(10)
            make.right.equalTo(-20)
        })
        
        addressLeftLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((creationTimeLeftLabel?.snp.bottom)!).offset(10)
            make.left.equalTo(imageWidth + 20)
            make.width.equalTo(65)
        })
        
        addressRightLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((creationTimeLeftLabel?.snp.bottom)!).offset(10)
            make.left.equalTo((addressLeftLabel?.snp.right)!).offset(10)
            make.right.equalTo(-20)
        })
        
        remarkeLeftLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((addressLeftLabel?.snp.bottom)!).offset(10)
            make.left.equalTo(imageWidth + 20)
            make.width.equalTo(65)
        })
        
        remarkRightLabel?.snp.makeConstraints({(make) in
            make.top.equalTo((addressLeftLabel?.snp.bottom)!).offset(10)
            make.left.equalTo((remarkeLeftLabel?.snp.right)!).offset(10)
            make.right.equalTo(-20)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
