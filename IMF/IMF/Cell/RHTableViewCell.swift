//
//  XZTableViewCell.swift
//  JiuCheYue
//
//  Created by 刘表聪 on 2018/3/29.
//  Copyright © 2018年 xinzhen. All rights reserved.
//

import UIKit
import SnapKit

class RHTableViewCell: UITableViewCell {
    
    var bottomLine: UIView?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .white
        self.bottomLine = self.xz_bottomBorderWithHeight(height: 1, color: RHAppSkinColor.sharedInstance.separatorColor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
