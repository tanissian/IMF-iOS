//
//  RHCityCollectionReusableView.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/13.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit
let headerIdentifier = "RHCityCollectionReusableView"

class RHCityCollectionReusableView: UICollectionReusableView {
    var label:UILabel!
    let width = UIScreen.main.bounds.size.width

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.label = UILabel.xz_labelWithFontSize(size: 14, textColor: RHAppSkinColor.sharedInstance.secondaryTextColor, backgroundColor: nil, numberOfLines: 1)
        self.label.frame = CGRect(x: 10, y: 6, width: width, height: 40)
        self.addSubview(self.label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
