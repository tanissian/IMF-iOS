import UIKit

class ProgressView: UIView {
    /// 灰色线条的颜色
    var strokelineWidth:CGFloat = 10.0
    /// 中间字的大小
    var numbelFont = UIFont.systemFont(ofSize: 18)
    /// 中间字的颜色
    var numbelTextColor = UIColor.black
    /// 内部轨道颜色
    var interiorRailwayColor = UIColor.lightGray
    /// 外部轨道颜色
    var exteriorRailwayColor = UIColor.red
    
    private var progressValue : CGFloat!
    private var progressFlag : CGFloat!
    var labelTimer : Timer?
    lazy var outLayer : CAShapeLayer! = {
        let Layer = CAShapeLayer()
        Layer.strokeColor = UIColor.lightGray.cgColor
        Layer.fillColor = UIColor.clear.cgColor
        Layer.lineCap = kCALineCapRound
        self.layer.addSublayer(Layer)
        return Layer
    }()
    
    lazy var progressLayer : CAShapeLayer! = {
        let layerNew = CAShapeLayer()
        layerNew.fillColor = UIColor.clear.cgColor
        layerNew.strokeColor = UIColor.red.cgColor
        self.layer.addSublayer(layerNew)
        return layerNew
    }()
    
    lazy var numberLabel : UILabel! = {
        let numLabelNew = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        numLabelNew.center = CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height/2)
        numLabelNew.backgroundColor = UIColor.clear
        numLabelNew.textAlignment = .center
        self.addSubview(numLabelNew)
        return numLabelNew
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        circleWithProgress(progress: 30, andIsAnimate: true)
    }
    /**
     调整绘制图像
     
     - parameter progress: 90
     - parameter animate:  true or false
     */
    func circleWithProgress(progress: CGFloat,andIsAnimate animate : Bool){
        if(animate){
            progressFlag = 0
            progressValue = progress
            // 灰色轨道
            let path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height / 2), radius: self.bounds.size.width / 2 - strokelineWidth / 2 - 5, startAngle: CGFloat(0) , endAngle: CGFloat( M_PI * 2) , clockwise: true)
            self.outLayer.path = path.cgPath
            self.outLayer.lineWidth = strokelineWidth
            self.outLayer.strokeColor = interiorRailwayColor.cgColor
            
            // run轨道
            let path1 = UIBezierPath(arcCenter: CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height / 2), radius: self.bounds.size.width/2 - strokelineWidth / 2 - 5 , startAngle: -CGFloat(M_PI * 0.5) , endAngle: CGFloat(M_PI * 1.5) , clockwise: true)
            self.progressLayer.path = path1.cgPath
            self.progressLayer.lineWidth = strokelineWidth
            self.progressLayer.strokeColor = exteriorRailwayColor.cgColor
            
            let pathAnima = CABasicAnimation(keyPath: "strokeEnd")
            pathAnima.duration = Double(progress / 100.0)
            pathAnima.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            pathAnima.fromValue = NSNumber(value: 0)
            pathAnima.toValue = NSNumber(value: Float(progress + 1 / 100.0))
            pathAnima.fillMode = kCAFillModeForwards
            pathAnima.isRemovedOnCompletion = false
            self.progressLayer.add(pathAnima, forKey: "strokeEndAnimation")
//            qqq()
            self.numberLabel.font = numbelFont
            self.numberLabel.textColor = numbelTextColor
            if(progress > 0){
                labelTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ProgressView.nameLbChange), userInfo: nil, repeats: true)
            }
        }else{
            self.numberLabel.text = "\(Int(progress))%"
            self.numberLabel.font = numbelFont
            self.numberLabel.textColor = numbelTextColor
            let path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height / 2), radius: self.bounds.size.width / 2 - strokelineWidth / 2 - 5, startAngle: CGFloat(0) , endAngle: CGFloat( M_PI * 2) , clockwise: true)
            self.outLayer.path = path.cgPath
            self.outLayer.lineWidth = strokelineWidth
            self.outLayer.strokeColor = interiorRailwayColor.cgColor
            
            let path1 = UIBezierPath(arcCenter:CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height / 2), radius: self.bounds.size.width/2 - strokelineWidth / 2 - 2.5 , startAngle: -CGFloat(M_PI * 0.5) , endAngle: CGFloat(M_PI * 1.5) , clockwise: true)
            self.progressLayer.path = path1.cgPath
            self.progressLayer.lineWidth = strokelineWidth + 5
            self.progressLayer.strokeEnd = progress / 100.0
            self.progressLayer.strokeColor = exteriorRailwayColor.cgColor
        }
    }
    
    func qqq()  {
        let progress = 20.0
        let path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height / 2), radius: self.bounds.size.width/2 - strokelineWidth / 2 - 5 , startAngle: -CGFloat(M_PI * 0.5) , endAngle: CGFloat(M_PI * 1.5) , clockwise: true)
        self.progressLayer.path = path.cgPath
        self.progressLayer.lineWidth = strokelineWidth
        self.progressLayer.strokeColor = UIColor.blue.cgColor
        
        let pathAnima = CABasicAnimation(keyPath: "strokeEnd")
//        pathAnima.duration = Double(progress / 100.0)
        pathAnima.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        pathAnima.fromValue = NSNumber(value: 0)
//        pathAnima.toValue = NSNumber(value: Float(progress / 100.0))
        pathAnima.fillMode = kCAFillModeForwards
        pathAnima.isRemovedOnCompletion = false
        self.progressLayer.add(pathAnima, forKey: "strokeEndAnimation")
    }
    /**
     定时器走的方法
     */
   @objc func nameLbChange(){
        if(progressFlag >= progressValue - 1){
            labelTimer!.invalidate()
            labelTimer = nil
        }
        progressFlag! += CGFloat(1.0)
        numberLabel.text = "\(Int(progressFlag))%"
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
