//
//  RHAPI.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/14.
//  Copyright © 2018年 imf. All rights reserved.
//

import Foundation

// 登录
let doLoginUrl = "/users/login"
// 获取验证码
let doSMSCodeUrl = "/sms"
// 获取热门城市列表
let doHotCityUrl = "/cities/hot"
// 搜索房源
let doSearchAssociateUrl = "/houses/typeahead"
// 房源图片
let doHousePhotoUrl = "/house/photo"
// 客户
let doClientsUrl = "/clients"
// 上传客户头像
let doClientsPhotoUrl = "/clients/photo/upload"
// 公共字典
let doDictsUrl = "/dicts"
// 个人信息
let doAccountsUrl = "/accounts"
// 房源类型
let doHouseTypeUrl = "/dicts/house_type"
// 详细房源搜索
let doHousesSearchUrl = "/houses/search"
// 分享组列表
let doShareGroupsUrl = "/share_groups"
