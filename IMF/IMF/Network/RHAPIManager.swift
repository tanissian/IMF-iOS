//
//  RHAPIManager.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/14.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHAPIManager: NSObject {
    static let sharedInstance: RHAPIManager = { RHAPIManager() }()
    let User_UIID = UIDevice.current.identifierForVendor?.uuidString

    override init() {
        super.init()
    }
    
    func doLogin(userName: String, msgCode: String, msgId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doLoginUrl, method: .post, header: nil, parameters: ["phone":userName, "msgCode":msgCode, "msgId":msgId], modelType: RHLoginResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doGetSMSCode(phone: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doSMSCodeUrl + "/" + phone, method: .get, header: nil, parameters: nil, modelType: RHSMSCodeResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doHotCity(result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doHotCityUrl, method: .get, header: nil, parameters: nil, modelType: RHHotCityModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doSearchAssociate(keyword: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doSearchAssociateUrl, method: .get, header: nil, parameters: ["keyword":keyword], modelType: RHSearchAssociateResponseModel.self, isEncryption: false, isData: true, success: result, failure: error)
    }
    
    func doHousePhoto(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doHousePhotoUrl + "/" + houseId, method: .get, header: nil, parameters: nil, modelType: RHSearchAssociateResponseModel.self, isEncryption: false, isData: true, success: result, failure: error)
    }
    
    func doAddClients(arrayData: Array<Any>, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        let parameters = ["name":arrayData[1],"phone":arrayData[2],"gender":1,
                          "birthDate":arrayData[4],"marital":0,"income":arrayData[6],
                          "jobTitle":arrayData[7],"jobType":arrayData[8],"email":arrayData[9],
                          "weixin":arrayData[10],"qq":arrayData[11],"investmentObjective":arrayData[12],
                          "capitalSource":arrayData[13],"tendencyRoom":arrayData[14],"tendencySite":arrayData[15],
                          "tendencyPrice":arrayData[16],"tendencySize":arrayData[17],"tendencySchool":arrayData[18],
                          "province":arrayData[19],"city":arrayData[20],"area":arrayData[21],
                          "address":arrayData[22],"remark":arrayData[23],"photo":arrayData[0]]
        
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doClientsUrl, method: .post, header: nil, parameters: parameters, modelType: RHResponseModel.self, isEncryption: false, isData: true, success: result, failure: error)
    }
    
    func doPutClients(arrayData: Array<Any>, clientId: Int, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        let parameters = ["name":arrayData[1],"phone":arrayData[2],"gender":1,
                          "birthDate":arrayData[4],"marital":0,"income":arrayData[6],
                          "jobTitle":arrayData[7],"jobType":arrayData[8],"email":arrayData[9],
                          "weixin":arrayData[10],"qq":arrayData[11],"investmentObjective":arrayData[12],
                          "capitalSource":arrayData[13],"tendencyRoom":arrayData[14],"tendencySite":arrayData[15],
                          "tendencyPrice":arrayData[16],"tendencySize":arrayData[17],"tendencySchool":arrayData[18],
                          "province":arrayData[19],"city":arrayData[20],"area":arrayData[21],
                          "address":arrayData[22],"remark":arrayData[23],"photo":arrayData[0]]
        
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doClientsUrl + "/" + String(clientId), method: .put, header: nil, parameters: parameters, modelType: RHResponseModel.self, isEncryption: false, isData: true, success: result, failure: error)
    }
    
    func doGetClients(pageNumber: String, pageSize: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doClientsUrl, method: .get, header: nil, parameters: ["pageNumber":pageNumber,"pageSize":pageSize], modelType: RHBuyersDetailsResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doGetClientsDetails(clientId: Int, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doClientsUrl + "/" + String(clientId), method: .get, header: nil, parameters: nil, modelType: RHBuyersDetailsDataResponseModel.self, isEncryption: false, isData: true, success: result, failure: error)
    }
    
    func doDeleteClients(clientId: Int, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doClientsUrl + "/" + String(clientId), method: .delete, header: nil, parameters: nil, modelType: RHResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doDicts(id: Int, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doDictsUrl + "/" + String(id), method: .get, header: nil, parameters: nil, modelType: RHDictsResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doAccounts(result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doAccountsUrl, method: .get, header: nil, parameters: nil, modelType: RHAccountResponseModel.self, isEncryption: false, isData: true, success: result, failure: error)
    }

    func doClientsPhoto(image: UIImage, fileName: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        let data = UIImage.zipImage(currentImage: image, scaleSize: 0.8, percent: 0.5)
        
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doClientsPhotoUrl, method: .post, header: nil, parameters: ["data":"data:image/jpeg;base64," + data.base64EncodedString(),"ext":"jpg"], modelType: RHHeadPortraitResoponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doHomesDetail(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        let sign = md5String(str: "ts=123&uuid=b190b5c7-94b4-482d-b300-14cfec6a2ce4c303701a92932475b516e5033b7a3e0b")
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/detail" + "?uuid=b190b5c7-94b4-482d-b300-14cfec6a2ce4&ts=123&sign=" + sign, method: .get, header: nil, parameters: nil, modelType: RHHousingDetailsModel.self, isEncryption: true, isData: true, success: result, failure: error)
    }
    
    func doGetRecommendHouse(clientId: String, pageNumber: String, pageSize: String,result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/clients/" + clientId + "/houses" + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize, method: .get, header: nil, parameters: nil, modelType: RHRecommendedHousingModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doAddRecommendHouse(clientId: String, houseId: String, remark: String,result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/clients/" + clientId + "/houses" + "?houseId=" + houseId + "&remark=" + remark, method: .post, header: nil, parameters: nil, modelType: RHRecommendedHousingModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doDeleteRecommendHouse(clientId: String, houseId: String,result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/clients/" + clientId + "/houses/"  + houseId, method: .delete, header: nil, parameters: nil, modelType: RHResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doHousePhotos(houseId: String,result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/houses/" + houseId + "/photos", method: .get, header: nil, parameters: nil, modelType: RHHousePhotosModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doHouseType(result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doHouseTypeUrl, method: .get, header: nil, parameters: nil, modelType: RHHouseTypeModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doShareGroups(ageNumber: Int, pageSize: Int, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doShareGroupsUrl, method: .get, header: nil, parameters: ["ageNumber":ageNumber,"pageSize":pageSize], modelType: RHShareGroupListModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doAddShareGroups(name: String, remark: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doShareGroupsUrl, method: .post, header: nil, parameters: ["remark":remark,"name":name,"status":"1"], modelType: RHResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doAddHousingShareGroups(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doShareGroupsUrl + "/" + houseId + "/houses", method: .post, header: nil, parameters: ["houseId":houseId], modelType: RHResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doDeleteShareGroups(shareId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doShareGroupsUrl + "/" + shareId, method: .delete, header: nil, parameters: nil, modelType: RHResponseModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doHousesSearch(action: String, keyword: String, page: Int, type: String, beds: Int, baths: Int, year: Int, minSize: Int, maxSize: Int, minPrice: Int, maxPrice: Int, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: doHousesSearchUrl, method: .get, header: nil, parameters: ["action":action, "keyword":keyword, "page":page, "pageSize":20, "type":type, "beds":beds, "baths":baths, "year":year, "minSize":minSize, "maxSize":maxSize, "minPrice":minPrice, "maxPrice":maxPrice, "filed":"livingArea", "direction":"asc"], modelType: RHHouseTypeModel.self, isEncryption: false, isData: false, success: result, failure: error)
    }
    
    func doHomeSchools(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/schools" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHSchoolsModel.self, isEncryption: true, isData: true, success: result, failure: error)
    }
    
    func doHomePoi(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?) {
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/poi" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHPoiModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingAge(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
         RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/age" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingAgeModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingCosts(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/costs" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingCostsModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingCrime(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/crime" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingCrimeModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingEducation(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/education" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingEducationModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingEmployment(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/employment" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingEmploymentModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingGender(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/gender" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingGenderModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingHousehold(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/household" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingHouseholdModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingIncome(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/income" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingIncomeModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingPopulation(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/population" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingPopulationModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func doCheckHousingSurroundingWeather(houseId: String, result: RHRequestSuccessCallback?, error: RHRequestFailureCallback?){
        RHNetworkManager.sharedInstance.requestWithEndpoint(endpoint: "/homes/" + houseId + "/weather" + getSign() + "&sign=" + getMd5Sign(), method: .get, header: nil, parameters: nil, modelType: RHHousingSurroundingWeatherModel.self, isEncryption: true, isData: false, success: result, failure: error)
    }
    
    func getMd5Sign() -> String{
        let string = "c303701a92932475b516e5033b7a3e0b"
        return md5String(str: "ts=123&uuid=" + User_UIID! + string)
    }
    
    func getSign() -> String{
        return "?uuid=" + User_UIID! + "&ts=123"
    }
    
    func md5String(str:String) -> String{
        let cStr = str.cString(using: String.Encoding.utf8);
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 16)
        CC_MD5(cStr!,(CC_LONG)(strlen(cStr!)), buffer)
        let md5String = NSMutableString();
        for i in 0 ..< 16{
            md5String.appendFormat("%02x", buffer[i])
        }
        free(buffer)
        return md5String as String
    }
}
