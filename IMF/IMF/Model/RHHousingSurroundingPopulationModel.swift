//
//  RHHousingSurroundingPopulationModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingPopulationModel: Codable {
    var data: RHHousingSurroundingPopulationResponseModel?
}

class RHHousingSurroundingPopulationResponseModel: Codable {
    var zip: RHHousingSurroundingPopulationZipModel?
}

class RHHousingSurroundingPopulationZipModel: Codable {
    var current: Float?
    var density: Float?
    var y1990: Float?
    var y2000: Float?
    var y2010: Float?
}
