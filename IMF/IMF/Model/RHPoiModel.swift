//
//  RHPoiModel.swift
//  IMF
//
//  Created by mac on 2018/10/16.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHPoiModel: Codable {
    var data:[RHPoiDataModel]?
}

class RHPoiDataModel: Codable {
    var address:String?
    var categories:Array<String>
    var latitude:Float?
    var longitude:Float?
    var name:String?
    var phone:String?
}

