//
//  RHShareGroupListModel.swift
//  IMF
//
//  Created by mac on 2018/10/11.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHShareGroupListModel: Codable {
    var data: [RHShareGroupListDataModel]?
}

class RHShareGroupListDataModel: Codable {
    var ctime: String?
    var name: String?
    var remark: String?
    var shareId: String?
    var status: Int?
    var utime: String?
}
