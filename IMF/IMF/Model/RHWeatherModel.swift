//
//  RHWeatherModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHWeatherModel: NSObject {
    var dataArray: NSMutableArray?
    var dataTitleArray: NSMutableArray?
    var indicesArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingWeatherModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingWeatherModel){
        dataArray = NSMutableArray()
        indicesArray = NSMutableArray()
        dataTitleArray = NSMutableArray()
        let annualArray = NSMutableArray()
        if data.data?.zip?.annual?.low != nil{
            annualArray.add(data.data?.zip?.annual?.low as Any)
            dataTitleArray?.add("annual")
        }
        if data.data?.zip?.annual?.high != nil{
            annualArray.add(data.data?.zip?.annual?.high as Any)
        }
        dataArray?.add(annualArray)
        
        let janArray = NSMutableArray()
        if data.data?.zip?.jan?.low != nil{
            janArray.add(data.data?.zip?.jan?.low as Any)
            dataTitleArray?.add("jan")
        }
        if data.data?.zip?.jan?.high != nil{
            janArray.add(data.data?.zip?.jan?.high as Any)
        }
        dataArray?.add(janArray)
        
        let aprArray = NSMutableArray()
        if data.data?.zip?.apr?.low != nil{
            aprArray.add(data.data?.zip?.apr?.low as Any)
            dataTitleArray?.add("low")
        }
        if data.data?.zip?.apr?.high != nil{
            aprArray.add(data.data?.zip?.apr?.high as Any)
        }
        dataArray?.add(aprArray)
        
        let julArray = NSMutableArray()
        if data.data?.zip?.jul?.low != nil{
            julArray.add(data.data?.zip?.jul?.low as Any)
            dataTitleArray?.add("jul")
        }
        if data.data?.zip?.jul?.high != nil{
            julArray.add(data.data?.zip?.jul?.high as Any)
        }
        dataArray?.add(julArray)
        
        let octArray = NSMutableArray()
        if data.data?.zip?.oct?.low != nil{
            octArray.add(data.data?.zip?.oct?.low as Any)
            dataTitleArray?.add("low")
        }
        if data.data?.zip?.oct?.high != nil{
            octArray.add(data.data?.zip?.oct?.high as Any)
        }
        dataArray?.add(octArray)

        if data.data?.zip?.indices?.earthquake != nil{
            indicesArray?.add(["text":"earthquake","data":data.data?.zip?.indices?.earthquake as Any])
        }
        if data.data?.zip?.indices?.hail != nil{
            indicesArray?.add(["text":"hail","data":data.data?.zip?.indices?.hail as Any])
        }
        if data.data?.zip?.indices?.hurricane != nil{
            indicesArray?.add(["text":"hurricane","data":data.data?.zip?.indices?.hurricane as Any])
        }
        if data.data?.zip?.indices?.overall != nil{
            indicesArray?.add(["text":"overall","data":data.data?.zip?.indices?.overall as Any])
        }
        if data.data?.zip?.indices?.ozone != nil{
            indicesArray?.add(["text":"ozone","data":data.data?.zip?.indices?.ozone as Any])
        }
        if data.data?.zip?.indices?.pollution != nil{
            indicesArray?.add(["text":"pollution","data":data.data?.zip?.indices?.pollution as Any])
        }
        if data.data?.zip?.indices?.tornado != nil{
            indicesArray?.add(["text":"tornado","data":data.data?.zip?.indices?.tornado as Any])
        }
        if data.data?.zip?.indices?.wind != nil{
            indicesArray?.add(["text":"wind","data":data.data?.zip?.indices?.wind as Any])
        }
    }
}
