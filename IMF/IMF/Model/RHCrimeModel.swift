//
//  RHCrimeModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHCrimeModel: NSObject {
    var dataArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingCrimeModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingCrimeModel){
        dataArray = NSMutableArray()
        if data.data?.zip?.assault != nil{
            dataArray?.add(["text":"assault","data":data.data?.zip?.assault as Any])
        }
        if data.data?.zip?.burglary != nil{
            dataArray?.add(["text":"burglary","data":data.data?.zip?.burglary as Any])
        }
        if data.data?.zip?.larceny != nil{
            dataArray?.add(["text":"larceny","data":data.data?.zip?.larceny as Any])
        }
        if data.data?.zip?.murder != nil{
            dataArray?.add(["text":"murder","data":data.data?.zip?.murder as Any])
        }
        if data.data?.zip?.overall != nil{
            dataArray?.add(["text":"overall","data":data.data?.zip?.overall as Any])
        }
        if data.data?.zip?.rape != nil{
            dataArray?.add(["text":"rape","data":data.data?.zip?.rape as Any])
        }
        if data.data?.zip?.robbery != nil{
            dataArray?.add(["text":"robbery","data":data.data?.zip?.robbery as Any])
        }
        if data.data?.zip?.vehicle != nil{
            dataArray?.add(["text":"vehicle","data":data.data?.zip?.vehicle as Any])
        }
    }
}
