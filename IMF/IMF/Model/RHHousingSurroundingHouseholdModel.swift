//
//  RHHousingSurroundingHouseholdModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingHouseholdModel: Codable {
    var data: RHHousingSurroundingHouseholdResponseModel?
}

class RHHousingSurroundingHouseholdResponseModel: Codable {
    var zip: RHHousingSurroundingHouseholdZipModel?
}

class RHHousingSurroundingHouseholdZipModel: Codable {
    var occupancy: RHHousingSurroundingHouseholdOccupancyModel?
    var duration: Float?
    var total: Float?
    var value: Float?
    var year_built: Float?
}

class RHHousingSurroundingHouseholdOccupancyModel: Codable {
    var children: Float?
    var families: Float?
    var married: Float?
    var owned: Float?
    var rented: Float?
    var vacant: Float?
}

