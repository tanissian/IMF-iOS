//
//  RHHousingSurroundingIncomeModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingIncomeModel: Codable {
    var data: RHHousingSurroundingIncomeResponseModel?
}

class RHHousingSurroundingIncomeResponseModel: Codable {
    var zip: RHHousingSurroundingIncomeZipModel?
}

class RHHousingSurroundingIncomeZipModel: Codable {
    var average: Float?
    var median: Float?
    var percent0_15: Float?
    var percent100_125: Float?
    var percent125_150: Float?
    var percent150_200: Float?
    var percent15_25: Float?
    var percent200_up: Float?
    var percent25_35: Float?
    var percent35_50: Float?
    var percent50_75: Float?
    var percent75_100: Float?
}
