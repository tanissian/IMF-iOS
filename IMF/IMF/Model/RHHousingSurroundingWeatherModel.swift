//
//  RHHousingSurroundingWeatherModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingWeatherModel: Codable {
    var data: RHHousingSurroundingWeatherResponseModel?
}

class RHHousingSurroundingWeatherResponseModel: Codable {
    var zip: RHHousingSurroundingWeatherZipModel?
}

class RHHousingSurroundingWeatherZipModel: Codable {
    var annual: RHHousingSurroundingTemperatureModel?
    var apr: RHHousingSurroundingTemperatureModel?
    var indices: RHHousingSurroundingIndicesModel?
    var jan: RHHousingSurroundingTemperatureModel?
    var jul: RHHousingSurroundingTemperatureModel?
    var oct: RHHousingSurroundingTemperatureModel?
    var precipitation: RHHousingSurroundingPrecipitationModel?
}

class RHHousingSurroundingTemperatureModel: Codable {
    var high: Float?
    var low: Float?
}

class RHHousingSurroundingIndicesModel: Codable {
    var earthquake: Int?
    var hail: Int?
    var hurricane: Int?
    var overall: Int?
    var ozone: Int?
    var pollution: Int?
    var tornado: Int?
    var wind: Int?
}

class RHHousingSurroundingPrecipitationModel: Codable {
    var rain: RHHousingSurroundingRainModel?
    var snow: RHHousingSurroundingSnowModel?
}

class RHHousingSurroundingRainModel: Codable {
    var amount: Float?
    var days_per_year: Float?
}

class RHHousingSurroundingSnowModel: Codable {
    var amount: Float?
    var days_per_year: Float?
}
