//
//  RHCostsModel.swift
//  IMF
//
//  Created by mac on 2018/10/20.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHCostsModel: NSObject {
    var dataArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingCostsModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingCostsModel){
        dataArray = NSMutableArray()
        if data.data?.zip?.apparel != nil{
            dataArray?.add(["text":"apparel","data":data.data?.zip?.apparel as Any])
        }
        if data.data?.zip?.education != nil{
            dataArray?.add(["text":"education","data":data.data?.zip?.education as Any])
        }
        if data.data?.zip?.entertainment != nil{
            dataArray?.add(["text":"entertainment","data":data.data?.zip?.entertainment as Any])
        }
        if data.data?.zip?.food != nil{
            dataArray?.add(["text":"food","data":data.data?.zip?.food as Any])
        }
        if data.data?.zip?.healthcare != nil{
            dataArray?.add(["text":"healthcare","data":data.data?.zip?.healthcare as Any])
        }
        if data.data?.zip?.housing != nil{
            dataArray?.add(["text":"housing","data":data.data?.zip?.housing as Any])
        }
        if data.data?.zip?.overall != nil{
            dataArray?.add(["text":"overall","data":data.data?.zip?.overall as Any])
        }
        if data.data?.zip?.transportation != nil{
            dataArray?.add(["text":"transportation","data":data.data?.zip?.transportation as Any])
        }
        if data.data?.zip?.utilities != nil{
            dataArray?.add(["text":"utilities","data":data.data?.zip?.utilities as Any])
        }
    }
}
