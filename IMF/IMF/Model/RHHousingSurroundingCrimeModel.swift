//
//  RHHousingSurroundingCrimeModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingCrimeModel: Codable {
    var data: RHHousingSurroundingCrimeResponseModel?
}

class RHHousingSurroundingCrimeResponseModel: Codable {
    var zip: RHHousingSurroundingCrimeZipModel?
}

class RHHousingSurroundingCrimeZipModel: Codable {
    var assault: Float?
    var burglary: Float?
    var larceny: Float?
    var murder: Float?
    var overall: Float?
    var rape: Float?
    var robbery: Float?
    var vehicle: Float?
}
