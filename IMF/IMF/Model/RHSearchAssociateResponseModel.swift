//
//  RHSearchAssociateResponseModel.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/19.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHSearchAssociateResponseModel: Codable {
    var address: [RHSearchAssociateDataAddressResponseModel]?
}

class RHSearchAssociateDataAddressResponseModel: Codable {
    // 房子ID
    var houseId: String?
    // 地址
    var address: String?
    // 照片
    var photo: String?
}
