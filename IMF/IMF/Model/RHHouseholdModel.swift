//
//  RHHouseholdModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHouseholdModel: NSObject {
    var dataArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingHouseholdModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingHouseholdModel){
        dataArray = NSMutableArray()
        if data.data?.zip?.occupancy?.children != nil{
            dataArray?.add(["text":"children","data":data.data?.zip?.occupancy?.children! as Any])
        }
        if data.data?.zip?.occupancy?.families != nil{
            dataArray?.add(["text":"families","data":data.data?.zip?.occupancy?.families! as Any])
        }
        if data.data?.zip?.occupancy?.married != nil{
            dataArray?.add(["text":"married","data":data.data?.zip?.occupancy?.married! as Any])
        }
        if data.data?.zip?.occupancy?.owned != nil{
            dataArray?.add(["text":"owned","data":data.data?.zip?.occupancy?.owned! as Any])
        }
        if data.data?.zip?.occupancy?.rented != nil{
            dataArray?.add(["text":"rented","data":data.data?.zip?.occupancy?.rented! as Any])
        }
        if data.data?.zip?.occupancy?.vacant != nil{
            dataArray?.add(["text":"vacant","data":data.data?.zip?.occupancy?.vacant! as Any])
        }
    }
}
