//
//  RHHousingSurroundingGenderModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingGenderModel: Codable {
    var data: RHHousingSurroundingGenderResponseModel?
}

class RHHousingSurroundingGenderResponseModel: Codable {
    var zip: RHHousingSurroundingGenderZipModel?
}

class RHHousingSurroundingGenderZipModel: Codable {
    var female: Float?
    var male: Float?

}
