//
//  RHDictsResponseModel.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHDictsResponseModel: Codable {
    var data: [RHDictsResponseDataModel]?
}

class RHDictsResponseDataModel: Codable {
    var name: String?
    var id: Int?
}

