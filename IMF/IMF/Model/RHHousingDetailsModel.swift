//
//  RHHousingDetailsModel.swift
//  IMF
//
//  Created by mac on 2018/10/9.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingDetailsModel: Codable {
    //价格
    var price: Float?
    //地址
    var address: String?
    //电器列表
    var appliances: Array<String>?
    //冷气系统
    var cooling_systems: Array<String>?
    //外部特色列表
    var exterior_types: Array<String>?
    //地板材料列表
    var floor_coverings: Array<String>?
    //供热燃料列表
    var heating_fuels: Array<String>?
    //供热系统列表
    var heating_systems: Array<String>?
    //停车类型列表
    var parking_types: Array<String>?
    //屋顶类型列表
    var roof_types: Array<String>?
    //风景列表
    var view_types: Array<String>?
    
    //建设单位数
    var building_unit_count: Int?
    //楼屋
    var condo_floor_num: Int?
    //浴室数量
    var full_bathrooms: Int?
    //1/2浴室
    var half_bathrooms: Int?
    //停车位
    var num_parking_spaces: Int?
    //1/4浴室
    var one_quarter_bathrooms: Int?
    //部分浴室
    var partial_bathrooms: Int?
    //3/4浴室
    var three_quarter_bathrooms: Int?
    //楼高
    var num_floors: Float?
    //lat
    var lat: Float?
    //lng
    var lng: Float?
    
    //房源描述
    var description: String?
    //MLS名称
    var mls_name: String?
    //MLS号码
    var mls_number: String?
    
    //是否有阁楼
    var has_attic: Bool?
    //是否有烧烤区
    var has_barbecue_area: Bool?
    //是否有地下室
    var has_basement: Bool?
    //是否有吊顶风扇
    var has_ceiling_fan: Bool?
    //是否有夹层
    var has_deck: Bool?
    //是否禁止访问
    var has_disabled_access: Bool?
    //是否有码头
    var has_dock: Bool?
    //是否有门卫
    var has_doorman: Bool?
    //是否有双层玻璃
    var has_double_pane_windows: Bool?
    //是否有电梯
    var has_elevator: Bool?
    //是否有壁炉
    var has_fireplace: Bool?
    //是否有花园
    var has_garden: Bool?
    //是否有安全闸门入门
    var has_gated_entry: Bool?
    //是否有温室
    var has_greenhouse: Bool?
    //是否有热浴缸
    var has_hot_tub_spa: Bool?
    //是否有喷水式浴缸
    var has_jetted_bath_tub: Bool?
    //是否有草坪
    var has_lawn: Bool?
    //是否有+++
    var has_mother_in_law: Bool?
    //是否有阳台
    var has_patio: Bool?
    //是否有池溏
    var has_pond: Bool?
    //是否有游泳池
    var has_pool: Bool?
    //是否有阳台
    var has_porch: Bool?
    //是否有房车停泊位
    var has_rv_parking: Bool?
    //是否有桑拿室
    var has_sauna: Bool?
    //是否有安全系统
    var has_security_system: Bool?
    //是否有天灯
    var has_skylight: Bool?
    //是否有体育馆
    var has_sports_court: Bool?
    //是否有喷淋消防系统
    var has_sprinkler_system: Bool?
    //是否有吊顶
    var has_vaulted_ceiling: Bool?
    //是否有酒水吧台
    var has_wet_bar: Bool?
    //是否有对讲机系统
    var intercom: Bool?
    //是否光纤接入
    var is_cable_ready: Bool?
    //是否是新建筑
    var is_new_construction: Bool?
    //是否是海边
    var is_waterfront: Bool?
    //是否是铺设钱路
    var is_wired: Bool?
    
    
    //开发商信息
    var builder: [RHHousingDetailsBuilderModel]?
    //相关费用
    var expenses: [RHHousingDetailsExpensesModel]?
    //税收信息
    var taxes: [RHHousingDetailsTaxesModel]?
}

class RHHousingDetailsBuilderModel: Codable {
    //开发商地址
    var address: String?
    //开发商邮箱
    var email: Int?
    //开发商名称
    var name: String?
    //开发商电话
    var phone: String?
    //开发商网址
    var url: String?
}

class RHHousingDetailsExpensesModel: Codable {
    //金额
    var amount: Float?
    //频率
    var interval: String?
    //类型
    var type: String?
}

class RHHousingDetailsTaxesModel: Codable {
    //金额
    var amount: Float?
    //年限
    var year: Int?
}
