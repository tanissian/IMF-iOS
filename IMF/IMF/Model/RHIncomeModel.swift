//
//  RHIncomeModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHIncomeModel: NSObject {
    var dataArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingIncomeModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingIncomeModel){
        dataArray = NSMutableArray()
        if data.data?.zip?.percent0_15 != nil{
            dataArray?.add(["text":"0-15k","data":data.data?.zip?.percent0_15 as Any])
        }
        if data.data?.zip?.percent15_25 != nil{
            dataArray?.add(["text":"15-25k","data":data.data?.zip?.percent15_25 as Any])
        }
        if data.data?.zip?.percent25_35 != nil{
            dataArray?.add(["text":"25-35k","data":data.data?.zip?.percent25_35 as Any])
        }
        if data.data?.zip?.percent35_50 != nil{
            dataArray?.add(["text":"35-50k","data":data.data?.zip?.percent35_50 as Any])
        }
        if data.data?.zip?.percent50_75 != nil{
            dataArray?.add(["text":"50-75k","data":data.data?.zip?.percent50_75 as Any])
        }
        if data.data?.zip?.percent75_100 != nil{
            dataArray?.add(["text":"75-100k","data":data.data?.zip?.percent75_100 as Any])
        }
        if data.data?.zip?.percent100_125 != nil{
            dataArray?.add(["text":"100-125k","data":data.data?.zip?.percent100_125 as Any])
        }
        if data.data?.zip?.percent125_150 != nil{
            dataArray?.add(["text":"125-150k","data":data.data?.zip?.percent125_150 as Any])
        }
        if data.data?.zip?.percent150_200 != nil{
            dataArray?.add(["text":"150-200k","data":data.data?.zip?.percent150_200 as Any])
        }
        if data.data?.zip?.percent200_up != nil{
            dataArray?.add(["text":"200k以上","data":data.data?.zip?.percent200_up as Any])
        }
    }
}
