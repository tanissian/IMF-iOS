//
//  RHBuyersDetailsResponseModel.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHBuyersDetailsResponseModel: Codable {
    var data: [RHBuyersDetailsDataResponseModel]?
}

class RHBuyersDetailsDataResponseModel: Codable {
    // 收入
    var income: String?
    // 县（区）
    var area: String?
    // 手机号
    var phone: String?
    // 微信
    var weixin: String?
    // 意向房价
    var tendencyPrice: String?
    // 省
    var province: String?
    // 婚姻
    var marital: Int?
    // 详细地址
    var address: String?
    // 投资目标
    var investmentObjective: String?
    // 资金来源
    var capitalSource: String?
    // 房间大小
    var tendencySize: String?
    // 工作类型
    var jobType: String?
    // 电子邮件
    var email: String?
    // qq
    var qq: String?
    // 城市
    var city: String?
    // 名字
    var name: String?
    // 备注
    var remark: String?
    // 用户ID
    var clientId: Int?
    // 生日
    var birthdate: String?
    // 职务
    var jobTitle: String?
    // 头像地址
    var photo: String?
    // 性别
    var gender: Int?
    // 房子类型
    var tendencyRoom: String?
    // 房子周边
    var tendencySite: String?
    // 学校倾向
    var tendencySchool: String?
}

