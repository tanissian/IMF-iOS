//
//  RHHouseTypeModel.swift
//  IMF
//
//  Created by mac on 2018/10/8.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHouseTypeModel: Codable {
    var data: [RHHouseTypeResponseDataModel]?
}

class RHHouseTypeResponseDataModel: Codable {
    var name: String?
    var id: Int?
    var explain: String?
}
