//
//  RHSchoolsModel.swift
//  IMF
//
//  Created by mac on 2018/10/16.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHSchoolsModel: Codable {
//    var data: [RHSchoolsDataModel]?
    var elementary:[RHSchoolsDetailsModel]?
    var high:[RHSchoolsDetailsModel]?
    var middle:[RHSchoolsDetailsModel]?
}

class RHSchoolsDataModel: Codable {
    var elementary:[RHSchoolsDetailsModel]?
    var high:[RHSchoolsDetailsModel]?
    var middle:[RHSchoolsDetailsModel]?
}

class RHSchoolsDetailsModel: Codable {
    var address:String?
    var grade:String?
    var id:String?
    var name:String?
    var type:String?
    var lat:Float?
    var lng:Float?
}
