//
//  RHSMSCodeResponseModel.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/19.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHSMSCodeResponseModel: Codable {
    // 短信 ID
    var data: String?
}
