//
//  RHAgeDModel.swift
//  IMF
//
//  Created by mac on 2018/10/19.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHAgeDModel: NSObject {
    var ageDataArray: NSMutableArray?
    var raceArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingAgeModel){
        
        if data.data?.zip?.age != nil {
            ageData(data: data)
        }
        if data.data?.zip?.race != nil {
            raceData(data: data)
        }
    }
    
    func raceData(data:RHHousingSurroundingAgeModel){
        raceArray = NSMutableArray()
        if data.data?.zip?.race!.asian != nil {
            raceArray?.add(["text":"asian","data":data.data?.zip?.race!.asian as Any])
        }
        if data.data?.zip?.race!.black != nil {
            raceArray?.add(["text":"black","data":data.data?.zip?.race!.black! as Any])
        }
        if data.data?.zip?.race!.natives != nil {
            raceArray?.add(["text":"natives","data":data.data?.zip?.race!.natives! as Any])
        }
        if data.data?.zip?.race!.hispanic != nil {
            raceArray?.add(["text":"hispanic","data":data.data?.zip?.race!.hispanic! as Any])
        }
        if data.data?.zip?.race!.pacific != nil {
            raceArray?.add(["text":"pacific","data":data.data?.zip?.race!.pacific! as Any])
        }
        if data.data?.zip?.race!.white != nil {
            raceArray?.add(["text":"white","data":data.data?.zip?.race!.white! as Any])
        }
    }

    func ageData(data:RHHousingSurroundingAgeModel){
        ageDataArray = NSMutableArray()

        if data.data?.zip?.age!.y0_5 != nil {
            ageDataArray?.add(["text":"0-5岁","data":data.data?.zip?.age!.y0_5! as Any])
        }
        if data.data?.zip?.age!.y6_11 != nil {
            ageDataArray?.add(["text":"6-11岁","data":data.data?.zip?.age!.y6_11! as Any])
        }
        if data.data?.zip?.age!.y12_17 != nil {
            ageDataArray?.add(["text":"12-17岁","data":data.data?.zip?.age!.y12_17! as Any])
        }
        if data.data?.zip?.age!.y18_24 != nil {
            ageDataArray?.add(["text":"18-24岁","data":data.data?.zip?.age!.y18_24! as Any])
        }
        if data.data?.zip?.age!.y25_34 != nil {
            ageDataArray?.add(["text":"25-34岁","data":data.data?.zip?.age!.y25_34! as Any])
        }
        if data.data?.zip?.age!.y35_44 != nil {
            ageDataArray?.add(["text":"35-44岁","data":data.data?.zip?.age!.y35_44! as Any])
        }
        if data.data?.zip?.age!.y45_54 != nil {
            ageDataArray?.add(["text":"45-54岁","data":data.data?.zip?.age!.y45_54! as Any])
        }
        if data.data?.zip?.age!.y55_64 != nil {
            ageDataArray?.add(["text":"55-64岁","data":data.data?.zip?.age!.y55_64! as Any])
        }
        if data.data?.zip?.age!.y65_74 != nil {
            ageDataArray?.add(["text":"65-74岁","data":data.data?.zip?.age!.y65_74! as Any])
        }
        if data.data?.zip?.age!.y75_84 != nil {
            ageDataArray?.add(["text":"75-84岁","data":data.data?.zip?.age!.y75_84! as Any])
        }
        if data.data?.zip?.age!.y85_up != nil {
            ageDataArray?.add(["text":"85岁以上","data":data.data?.zip?.age!.y85_up! as Any])
        }
    }
}
