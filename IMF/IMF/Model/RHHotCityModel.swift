//
//  RHHotCityModel.swift
//  IMF
//
//  Created by 黄瑞东 on 2018/9/19.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHotCityModel: Codable {
    var data: [RHHotCityResponseModel]?
}

class RHHotCityResponseModel: Codable {
    // 中文城市名
    var enName: String?
    // 英文城市名
    var chName: String?
}
