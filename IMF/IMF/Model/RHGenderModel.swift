//
//  RHGenderModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHGenderModel: NSObject {
    var dataArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingGenderModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingGenderModel){
        dataArray = NSMutableArray()
        if data.data?.zip?.female != nil{
            dataArray?.add(["text":"female","data":data.data?.zip?.female as Any])
        }
        if data.data?.zip?.male != nil{
            dataArray?.add(["text":"male","data":data.data?.zip?.male as Any])
        }
    }
}
