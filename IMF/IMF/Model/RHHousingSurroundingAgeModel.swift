//
//  RHHousingSurroundingAgeModel.swift
//  IMF
//
//  Created by mac on 2018/10/19.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingAgeModel: Codable {
    var data: RHHousingSurroundingAgeResponseModel?
}

class RHHousingSurroundingAgeResponseModel: Codable {
    var zip: RHHousingSurroundingAgeZipModel?
}

class RHHousingSurroundingAgeZipModel: Codable {
    var median: Float?
    var age: RHHousingSurroundingAgeDataModel?
    var gender: RHHousingSurroundingGenderItemModel?
    var race: RHHousingSurroundingRaceItemModel?
}

class RHHousingSurroundingAgeDataModel: Codable {
    var y0_5: Float?
    var y6_11: Float?
    var y12_17: Float?
    var y18_24: Float?
    var y25_34: Float?
    var y35_44: Float?
    var y45_54: Float?
    var y55_64: Float?
    var y65_74: Float?
    var y75_84: Float?
    var y85_up: Float?
}

class RHHousingSurroundingGenderItemModel: Codable {
    var female: Float?
    var male: Float?
}

class RHHousingSurroundingRaceItemModel: Codable {
    var asian: Float?
    var black: Float?
    var hispanic: Float?
    var natives: Float?
    var pacific: Float?
    var white: Float?
}
