//
//  RHEducationModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHEducationModel: NSObject {
    var dataArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingEducationModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingEducationModel){
        dataArray = NSMutableArray()
        if data.data?.zip?.associate != nil{
            dataArray?.add(["text":"associate","data":data.data?.zip?.associate as Any])
        }
        if data.data?.zip?.bachelor != nil{
            dataArray?.add(["text":"bachelor","data":data.data?.zip?.bachelor as Any])
        }
        if data.data?.zip?.graduate != nil{
            dataArray?.add(["text":"graduate","data":data.data?.zip?.graduate as Any])
        }
        if data.data?.zip?.highschool != nil{
            dataArray?.add(["text":"highschool","data":data.data?.zip?.highschool as Any])
        }
    }
}
