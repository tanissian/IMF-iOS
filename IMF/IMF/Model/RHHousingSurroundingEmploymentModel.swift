//
//  RHHousingSurroundingEmploymentModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingEmploymentModel: Codable {
    var data: RHHousingSurroundingEmploymentResponseModel?
}

class RHHousingSurroundingEmploymentResponseModel: Codable {
    var zip: RHHousingSurroundingEmploymentZipModel?
}

class RHHousingSurroundingEmploymentZipModel: Codable {
    var accommodations: Float?
    var administration: Float?
    var agriculture: Float?
    var arts: Float?
    var construction: Float?
    var education: Float?
    var finance: Float?
    var government: Float?
    var healthcare: Float?
    var management: Float?
    var manufacturing: Float?
    var mining: Float?
    var other: Float?
    var professional: Float?
    var realestate: Float?
    var retail: Float?
    var technology: Float?
    var transportation: Float?
    var utilities: Float?
    var wholesale: Float?
}
