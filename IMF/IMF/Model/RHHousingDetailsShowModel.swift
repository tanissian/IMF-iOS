//
//  RHHousingDetailsShowModel.swift
//  IMF
//
//  Created by mac on 2018/10/9.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingDetailsShowModel: NSObject {
    
    var leftDataArray = NSMutableArray()
    var rightDataArray = NSMutableArray()
    var count = 0
    var rowsCount = NSMutableArray()
    var lat = Float()
    var lng = Float()
    func RHGetDetailsData(model:RHHousingDetailsModel) {
        lat = model.lat == nil ? 0 : model.lat!
        lng = model.lng == nil ? 0 : model.lng!
        RHAddLeftDataArray(model: model)
        RHAddRightDataArray(model: model)
    }
    
    func RHAddRightDataArray(model:RHHousingDetailsModel) {
        let arrayList = NSMutableArray()
        let arrayList2 = NSMutableArray()
        let arrayList3 = NSMutableArray()
        
        arrayList.add(model.price != nil ? String(model.price!) : "")
        arrayList.add(model.address != nil ? String(model.address!) : "")

        arrayList.add(model.building_unit_count != nil ? String(model.building_unit_count!) : "")
        arrayList.add(model.condo_floor_num != nil ? String(model.condo_floor_num!) : "")
        arrayList.add(model.full_bathrooms != nil ? String(model.full_bathrooms!) : "")
        arrayList.add(model.half_bathrooms != nil ? String(model.half_bathrooms!) : "")
        arrayList.add(model.one_quarter_bathrooms != nil ? String(model.one_quarter_bathrooms!) : "")
        arrayList.add(model.num_parking_spaces != nil ? String(model.num_parking_spaces!) : "")
        arrayList.add(model.partial_bathrooms != nil ? String(model.partial_bathrooms!) : "")
        arrayList.add(model.three_quarter_bathrooms != nil ? String(model.three_quarter_bathrooms!) : "")
        arrayList.add(model.num_floors != nil ? String(model.num_floors!) : "")
        arrayList.add(model.mls_name != nil ? String(model.mls_name!) : "")
        arrayList.add(model.mls_number != nil ? String(model.mls_number!) : "")
        
        arrayList.add(model.has_attic != nil ? BoolConversionString(bool: model.has_attic!) : "")
        arrayList.add(model.has_barbecue_area != nil ? BoolConversionString(bool: model.has_barbecue_area!) : "")
        arrayList.add(model.has_basement != nil ?BoolConversionString(bool: model.has_basement!) : "")
        arrayList.add(model.has_ceiling_fan != nil ? BoolConversionString(bool: model.has_ceiling_fan!) : "")
        arrayList.add(model.has_deck != nil ? BoolConversionString(bool: model.has_deck!) : "")
        arrayList.add(model.has_disabled_access != nil ? BoolConversionString(bool: model.has_disabled_access!) : "")
        arrayList.add(model.has_dock != nil ? BoolConversionString(bool: model.has_dock!) : "")
        arrayList.add(model.has_doorman != nil ? BoolConversionString(bool: model.has_doorman!) : "")
        arrayList.add(model.has_double_pane_windows != nil ? BoolConversionString(bool: model.has_double_pane_windows!) : "")
        arrayList.add(model.has_elevator != nil ? BoolConversionString(bool: model.has_elevator!) : "")
        arrayList.add(model.has_fireplace != nil ? BoolConversionString(bool: model.has_fireplace!) : "")
        arrayList.add(model.has_garden != nil ? BoolConversionString(bool: model.has_garden!) : "")
        arrayList.add(model.has_gated_entry != nil ? BoolConversionString(bool: model.has_gated_entry!) : "")
        arrayList.add(model.has_greenhouse != nil ? BoolConversionString(bool: model.has_greenhouse!) : "")
        arrayList.add(model.has_hot_tub_spa != nil ? BoolConversionString(bool: model.has_hot_tub_spa!) : "")
        arrayList.add(model.has_jetted_bath_tub != nil ? BoolConversionString(bool: model.has_jetted_bath_tub!) : "")
        arrayList.add(model.has_lawn != nil ? BoolConversionString(bool: model.has_lawn!) : "")
        arrayList.add(model.has_mother_in_law != nil ? BoolConversionString(bool: model.has_mother_in_law!) : "")
        arrayList.add(model.has_patio != nil ? BoolConversionString(bool: model.has_patio!) : "")
        arrayList.add(model.has_pond != nil ? BoolConversionString(bool: model.has_pond!) : "")
        arrayList.add(model.has_pool != nil ? BoolConversionString(bool: model.has_pool!) : "")
        arrayList.add(model.has_porch != nil ? BoolConversionString(bool: model.has_porch!) : "")
        arrayList.add(model.has_rv_parking != nil ? BoolConversionString(bool: model.has_rv_parking!) : "")
        arrayList.add(model.has_sauna != nil ? BoolConversionString(bool: model.has_sauna!) : "")
        arrayList.add(model.has_security_system != nil ? BoolConversionString(bool: model.has_security_system!) : "")
        arrayList.add(model.has_skylight != nil ? BoolConversionString(bool: model.has_skylight!) : "")
        arrayList.add(model.has_sports_court != nil ? BoolConversionString(bool: model.has_sports_court!) : "")
        arrayList.add(model.has_sprinkler_system != nil ? BoolConversionString(bool: model.has_sprinkler_system!) : "")
        arrayList.add(model.has_vaulted_ceiling != nil ? BoolConversionString(bool: model.has_vaulted_ceiling!) : "")
        arrayList.add(model.has_wet_bar != nil ? BoolConversionString(bool: model.has_wet_bar!) : "")
        arrayList.add(model.intercom != nil ? BoolConversionString(bool: model.intercom!) : "")
        arrayList.add(model.is_cable_ready != nil ? BoolConversionString(bool: model.is_cable_ready!) : "")
        arrayList.add(model.is_new_construction != nil ? BoolConversionString(bool: model.is_new_construction!) : "")
        arrayList.add(model.is_waterfront != nil ? BoolConversionString(bool: model.is_waterfront!) : "")
        arrayList.add(model.is_wired != nil ? BoolConversionString(bool: model.is_wired!) : "")
        
        rowsCount.add(filterData(array: arrayList).count)
        if model.appliances != nil {arrayList2.add(model.appliances!)}
        if model.cooling_systems != nil {arrayList2.add(model.cooling_systems!)}
        if model.exterior_types != nil {arrayList2.add(model.exterior_types!)}
        if model.floor_coverings != nil {arrayList2.add(model.floor_coverings!)}
        if model.heating_fuels != nil {arrayList2.add(model.heating_fuels!)}
        if model.heating_systems != nil {arrayList2.add(model.heating_systems!)}
        if model.parking_types != nil {arrayList2.add(model.parking_types!)}
        if model.roof_types != nil {arrayList2.add(model.roof_types!)}
        if model.view_types != nil {arrayList2.add(model.view_types!)}

        for index in arrayList2{
            rowsCount.add((index as! NSArray).count)
        }
        
        if model.builder != nil {
            let array = NSMutableArray()
            array.add(model.builder![0].address != nil ? model.builder![0].address! : "")
            array.add(model.builder![0].email != nil ? String(model.builder![0].email!) : "")
            array.add(model.builder![0].name != nil ? model.builder![0].name! : "")
            array.add(model.builder![0].phone != nil ? model.builder![0].phone! : "")
            array.add(model.builder![0].url != nil ? model.builder![0].url! : "")
            rowsCount.add(array.count)
            arrayList3.add(array)
        }
        
        if model.expenses != nil {
            let array = NSMutableArray()
            array.add(model.expenses![0].amount != nil ? String(model.expenses![0].amount!) : "")
            array.add(model.expenses![0].interval != nil ? model.expenses![0].interval! : "")
            array.add(model.expenses![0].type != nil ? model.expenses![0].type! : "")
            rowsCount.add(array.count)
            arrayList3.add(array)
        }
        
        if model.taxes != nil {
            let array = NSMutableArray()
            array.add(model.taxes![0].amount != nil ? String(model.taxes![0].amount!) : "")
            array.add(model.taxes![0].year != nil ? String(model.taxes![0].year!) : "")
            rowsCount.add(array.count)
            arrayList3.add(array)
        }
        
        if model.description != nil {
            let array = NSMutableArray()
            array.add(model.description!)
            rowsCount.add(array.count)
            arrayList3.add(array)
        }
        
        rightDataArray.add(filterData(array: arrayList))
        rightDataArray.add(filterData(array: arrayList2))
        rightDataArray.add(filterData(array: arrayList3))
    }
    
    func RHAddLeftDataArray(model:RHHousingDetailsModel) {
        let arrayList = NSMutableArray()
        let arrayList2 = NSMutableArray()
        let arrayList3 = NSMutableArray()

        arrayList.add(model.price != nil ? "价格" : "")
        arrayList.add(model.address != nil ? "地址" : "")
        arrayList.add(model.building_unit_count != nil ? "建设单位数量" : "")
        arrayList.add(model.condo_floor_num != nil ? "楼屋数量" : "")
        arrayList.add(model.full_bathrooms != nil ? "浴室数量" : "")
        arrayList.add(model.half_bathrooms != nil ? "1/2浴室" : "")
        arrayList.add(model.one_quarter_bathrooms != nil ? "1/4浴室" : "")
        arrayList.add(model.num_parking_spaces != nil ? "停车位" : "")
        arrayList.add(model.partial_bathrooms != nil ? "部分浴室" : "")
        arrayList.add(model.three_quarter_bathrooms != nil ? "3/4浴室" : "")
        arrayList.add(model.num_floors != nil ? "楼高" : "")
        arrayList.add(model.mls_name != nil ? "MLS名称" : "")
        arrayList.add(model.mls_number != nil ? "MLS号码" : "")
        
        arrayList.add(model.has_attic != nil ? "阁楼" : "")
        arrayList.add(model.has_barbecue_area != nil ? "烧烤区" : "")
        arrayList.add(model.has_basement != nil ? "地下室" : "")
        arrayList.add(model.has_ceiling_fan != nil ? "吊顶风扇" : "")
        arrayList.add(model.has_deck != nil ? "夹层" : "")
        arrayList.add(model.has_disabled_access != nil ? "禁止访问" : "")
        arrayList.add(model.has_dock != nil ? "码头" : "")
        arrayList.add(model.has_doorman != nil ? "门卫" : "")
        arrayList.add(model.has_double_pane_windows != nil ? "双层玻璃" : "")
        arrayList.add(model.has_elevator != nil ? "电梯" : "")
        arrayList.add(model.has_fireplace != nil ? "壁炉" : "")
        arrayList.add(model.has_garden != nil ? "花园" : "")
        arrayList.add(model.has_gated_entry != nil ? "安全闸门入门" : "")
        arrayList.add(model.has_greenhouse != nil ? "温室" : "")
        arrayList.add(model.has_hot_tub_spa != nil ? "热浴缸" : "")
        arrayList.add(model.has_jetted_bath_tub != nil ? "喷水式浴缸" : "")
        arrayList.add(model.has_lawn != nil ? "草坪" : "")
        arrayList.add(model.has_mother_in_law != nil ? "+++" : "")
        arrayList.add(model.has_patio != nil ? "阳台" : "")
        arrayList.add(model.has_pond != nil ? "池溏" : "")
        arrayList.add(model.has_pool != nil ? "游泳池" : "")
        arrayList.add(model.has_porch != nil ? "阳台" : "")
        arrayList.add(model.has_rv_parking != nil ? "房车停泊位" : "")
        arrayList.add(model.has_sauna != nil ? "桑拿室" : "")
        arrayList.add(model.has_security_system != nil ? "安全系统" : "")
        arrayList.add(model.has_skylight != nil ? "天灯" : "")
        arrayList.add(model.has_sports_court != nil ? "体育馆" : "")
        arrayList.add(model.has_sprinkler_system != nil ? "喷淋消防系统" : "")
        arrayList.add(model.has_vaulted_ceiling != nil ? "吊顶" : "")
        arrayList.add(model.has_wet_bar != nil ? "酒水吧台" : "")
        arrayList.add(model.intercom != nil ? "对讲机系统" : "")
        arrayList.add(model.is_cable_ready != nil ? "光纤接入" : "")
        arrayList.add(model.is_new_construction != nil ? "新建筑" : "")
        arrayList.add(model.is_waterfront != nil ? "海边" : "")
        arrayList.add(model.is_wired != nil ? "铺设钱路" : "")
        
        arrayList2.add(model.appliances != nil ? "电器列表" : "")
        arrayList2.add(model.cooling_systems != nil ? "冷气系统" : "")
        arrayList2.add(model.exterior_types != nil ? "外部特色" : "")
        arrayList2.add(model.floor_coverings != nil ? "地板材料" : "")
        arrayList2.add(model.heating_fuels != nil ? "供热燃料" : "")
        arrayList2.add(model.heating_systems != nil ? "供热系统" : "")
        arrayList2.add(model.parking_types != nil ? "停车类型" : "")
        arrayList2.add(model.roof_types != nil ? "屋顶类型" : "")
        arrayList2.add(model.view_types != nil ? "周围风景" : "")
        if model.builder != nil {
            let array = NSMutableArray()
            array.add(model.builder != nil ? "开发商信息" : "")
            array.add(model.builder![0].address != nil ? "开发商地址" : "")
            array.add(model.builder![0].email != nil ? "开发商邮箱" : "")
            array.add(model.builder![0].name != nil ? "开发商名称" : "")
            array.add(model.builder![0].phone != nil ? "开发商电话" : "")
            array.add(model.builder![0].url != nil ? "开发商网址" : "")
            arrayList3.add(array)
        }
        
        if model.expenses != nil {
            let array = NSMutableArray()
            array.add(model.expenses != nil ? "相关费用" : "")
            array.add(model.expenses![0].amount != nil ? "金额" : "")
            array.add(model.expenses![0].interval != nil ? "缴费周期" : "")
            array.add(model.expenses![0].type != nil ? "类型" : "")
            arrayList3.add(array)
        }
        
        if model.taxes != nil {
            let array = NSMutableArray()
            array.add(model.taxes != nil ? "税收信息" : "")
            array.add(model.taxes![0].amount != nil ? "金额" : "")
            array.add(model.taxes![0].year != nil ? "年限" : "")
            arrayList3.add(array)
        }
        
        if model.description != nil {
            let array = NSMutableArray()
            array.add("描述")
            arrayList3.add(array)
        }

        leftDataArray.add(filterData(array: arrayList))
        leftDataArray.add(filterData(array: arrayList2))
        leftDataArray.add(filterData(array: arrayList3))
    }
    
    func filterData(array: NSMutableArray) -> NSMutableArray{
        var i = 0
        for _ in array{
            if array[i] as? String == ""{
                array.removeObject(at: i)
                i = i - 1
            }
            i = i + 1
        }
        return array
    }
    
    func BoolConversionString(bool: Bool) -> String{
        return bool == true ? "是" : "否"
    }
}
