//
//  RHPopulationModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHPopulationModel: NSObject {
    var dataArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingPopulationModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingPopulationModel){
        dataArray = NSMutableArray()
        if data.data?.zip?.current != nil{
            dataArray?.add(["text":"current","data":data.data?.zip?.current as Any])
        }
        if data.data?.zip?.density != nil{
            dataArray?.add(["text":"density","data":data.data?.zip?.density as Any])
        }
        if data.data?.zip?.y1990 != nil{
            dataArray?.add(["text":"y1990","data":data.data?.zip?.y1990 as Any])
        }
        if data.data?.zip?.y2000 != nil{
            dataArray?.add(["text":"y2000","data":data.data?.zip?.y2000 as Any])
        }
        if data.data?.zip?.y2010 != nil{
            dataArray?.add(["text":"y2010","data":data.data?.zip?.y2010 as Any])
        }
    }
}
