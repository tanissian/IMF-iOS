//
//  RHHousingSurroundingEducationModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingEducationModel: Codable {
    var data: RHHousingSurroundingEducationResponseModel?
}

class RHHousingSurroundingEducationResponseModel: Codable {
    var zip: RHHousingSurroundingEducationZipModel?
}

class RHHousingSurroundingEducationZipModel: Codable {
    var associate: Float?
    var bachelor: Float?
    var graduate: Float?
    var highschool: Float?
}
