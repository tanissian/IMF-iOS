//
//  RHHousingSurroundingCostsModel.swift
//  IMF
//
//  Created by mac on 2018/10/20.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHHousingSurroundingCostsModel: Codable {
    var data: RHHousingSurroundingCostsResponseModel?
}

class RHHousingSurroundingCostsResponseModel: Codable {
    var zip: RHHousingSurroundingCostsZipModel?
}

class RHHousingSurroundingCostsZipModel: Codable {
    var entertainment: Float?
    var overall: Float?
    var apparel: Float?
    var healthcare: Float?
    var utilities: Float?
    var transportation: Float?
    var education: Float?
    var housing: Float?
    var food: Float?
}

