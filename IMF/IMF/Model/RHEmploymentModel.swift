//
//  RHEmploymentModel.swift
//  IMF
//
//  Created by mac on 2018/10/21.
//  Copyright © 2018年 imf. All rights reserved.
//

import UIKit

class RHEmploymentModel: NSObject {
    var dataArray: NSMutableArray?
    func allocData(data:RHHousingSurroundingEmploymentModel){
        if data.data?.zip != nil {
            costsData(data: data)
        }
    }
    
    func costsData(data:RHHousingSurroundingEmploymentModel){
        dataArray = NSMutableArray()
        if data.data?.zip?.accommodations != nil{
            dataArray?.add(["text":"accommodations","data":data.data?.zip?.accommodations as Any])
        }
        if data.data?.zip?.administration != nil{
            dataArray?.add(["text":"administration","data":data.data?.zip?.administration as Any])
        }
        if data.data?.zip?.agriculture != nil{
            dataArray?.add(["text":"agriculture","data":data.data?.zip?.agriculture as Any])
        }
        if data.data?.zip?.arts != nil{
            dataArray?.add(["text":"arts","data":data.data?.zip?.arts as Any])
        }
        if data.data?.zip?.construction != nil{
            dataArray?.add(["text":"construction","data":data.data?.zip?.construction as Any])
        }
        if data.data?.zip?.education != nil{
            dataArray?.add(["text":"education","data":data.data?.zip?.education as Any])
        }
        if data.data?.zip?.finance != nil{
            dataArray?.add(["text":"finance","data":data.data?.zip?.finance as Any])
        }
        if data.data?.zip?.government != nil{
            dataArray?.add(["text":"government","data":data.data?.zip?.government as Any])
        }
        if data.data?.zip?.healthcare != nil{
            dataArray?.add(["text":"healthcare","data":data.data?.zip?.healthcare as Any])
        }
        if data.data?.zip?.management != nil{
            dataArray?.add(["text":"management","data":data.data?.zip?.management as Any])
        }
        if data.data?.zip?.manufacturing != nil{
            dataArray?.add(["text":"manufacturing","data":data.data?.zip?.manufacturing as Any])
        }
        if data.data?.zip?.mining != nil{
            dataArray?.add(["text":"mining","data":data.data?.zip?.mining as Any])
        }
        if data.data?.zip?.other != nil{
            dataArray?.add(["text":"other","data":data.data?.zip?.other as Any])
        }
        if data.data?.zip?.professional != nil{
            dataArray?.add(["text":"professional","data":data.data?.zip?.professional as Any])
        }
        if data.data?.zip?.realestate != nil{
            dataArray?.add(["text":"realestate","data":data.data?.zip?.realestate as Any])
        }
        if data.data?.zip?.retail != nil{
            dataArray?.add(["text":"retail","data":data.data?.zip?.retail as Any])
        }
        if data.data?.zip?.technology != nil{
            dataArray?.add(["text":"technology","data":data.data?.zip?.technology as Any])
        }
        if data.data?.zip?.transportation != nil{
            dataArray?.add(["text":"transportation","data":data.data?.zip?.transportation as Any])
        }
        if data.data?.zip?.utilities != nil{
            dataArray?.add(["text":"utilities","data":data.data?.zip?.utilities as Any])
        }
        if data.data?.zip?.wholesale != nil{
            dataArray?.add(["text":"wholesale","data":data.data?.zip?.wholesale as Any])
        }
    }
}
